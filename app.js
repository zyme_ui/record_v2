/**
 * Created by Rainc on 14-12-9.
 */
var express = require('express');
var session = require('express-session');
var app = module.exports = express();
config = require('./etc/config.json');
server_domain_uuid =0 ;
var fs = require('fs');
var exec = require('child_process').exec;
var appServer = require(config.appServerPath);
var fileCtrl = require('./controllers/fileWatch.js');
var stateCtrl = require('./controllers/stateCtrl.js');
var router = require('./routers');
var log4js = require('log4js');
var bodyParser = require('body-parser');
var db = require('./controllers/db_pool');
var mysql1 = require('./controllers/basedao.js');
var lanCon = require('./controllers/lanCon.js');
var global = require('./controllers/global.js').global;

log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.webLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'WEBSERVER'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

product_id = config.product_id;
server_id = config.server_id;
recording_path = config.recording_path || '';
status_path = config.status_path || '';
backup_path = config.backup_path || '';
account = config.account;                 //读取默认用户名
password = config.password;               //读取默认密码

//app.configure
//app.use(log4js.connectLogger(logger, {level:'auto', format:':remote-addr :method :url'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.use(express.static(config.cache_path));
app.use(express.static(recording_path));
app.use(express.static(backup_path));
app.set('views', __dirname + '/public');
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.use(session({
  secret: 'record',
  cookie: { maxAge: 6000 * 1000 }
}));

//路由
router(app);

//默认初始语言为中文
localLan = 'zh_CN';

var pidFilePath = config.pidFilePath;
fs.open(pidFilePath,'a+',function(err){
  if(err){
    logger.error(err);
  }else{
    fs.readFile(pidFilePath, 'utf8', function (err, data) {
      if (err) {
        logger.error(err);
      }else if(!data){
        runApp();
      }else{
        var cmd = "ps -ax | grep "+ data + " | awk '{ print $1 }'";
        var isPidActive = false;
        exec(cmd,
          function (error, stdout, stderr) {
            if (error !== null) {
              logger.error('exec error: ' + error);
            }else if(stdout){
              var result = stdout.split('\n');
              for(var i in result){
                if(data == result[i]){
                  isPidActive = true;
                  break;
                }
              }
              if(isPidActive == false){
                runApp();
              }else{
                logger.info("record server is running!Please stop it before to run another one!");
                process.exit();
              }
            }else{
              runApp();
            }
          });
      }
    })
  }
})

function runApp(){
  //启动web server
  app.listen(config.web_port);
  logger.info("server listening on port %d in %s mode", config.web_port, app.settings.env);
  logger.info("You can debug your app with http://" + config.server_ip + ':' + config.web_port);

  //判断是否连接上数据库，若未连上，5秒持续尝试连接，直到连接上，才开启子进程监听端口,
  var checkConnectedToDatabase = setInterval(function(){
    db.handleDisconnect(function(err,con){
      if(!err && con){
        //若连接cloud查询获取cloud域id
        if(config.connect_to_cloud == 'true'){
          var sql = "select cloud_uuid from tbl_sys where uuid=" + config.server_id;
          mysql1.find(sql,function(err,data){
            if(err){
              logger.error('查询cloud域id失败！');
              //当加载出错时，发出严重告警
              global.createAlarm(server_domain_uuid,2,1,5,'CLOUD_ALARM',lanCon.getLanValue('query_domain_id_error'),global.localStaticToUTC(new Date().getTime()));
            }else if(data.length ==0){
              logger.error('cloud查无该服务id所对应域id！');
              //当加载出错时，发出严重告警
              global.createAlarm(server_domain_uuid,2,1,5,'CLOUD_ALARM',lanCon.getLanValue('no_domain_id'),global.localStaticToUTC(new Date().getTime()));
            }else{
              server_domain_uuid = row[0].cloud_uuid;
              startOperation();
            }
          })
        }else{
          startOperation();
        }
        clearInterval(checkConnectedToDatabase);
      }
    });
  },5000);

  //将当前pid写入pid文件中
  var pid = process.pid;
  fs.writeFile(pidFilePath, pid, function (err) {
    if (err){
      logger.error('启动写入pid文件出错！');
    }else{
      logger.info('Pid it\'s saved!'); //文件被保存
    }
  });
}

function startOperation(){
  appServer.init(function(err,obj){
    if(!err){
      server_version = obj.server_version || '';

      fileCtrl.saveToMemory();
      fileCtrl.watchFile();
      stateCtrl.insertIntoTable();       //将状态信息写入数据库
    }else{
      logger.error(err);
    }
  });
}


