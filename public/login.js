/**
 * Created by Rainc on 2015/5/26.
 */
function init(lan){
  //初始化语言
  window.lanEn = {
    'Network error!Please retry!':'Network error!Please retry!',
    'RECORD LOGIN':'RECORD LOGIN',
    'Username':'Username',
    'Password':'Password',
    'Remember me':'Remember me',
    'LOGIN':'LOGIN',
    'User Alias':'User Alias',
    'Confirm Password':'Confirm Password',
    'Register success!Please login!':'Register success!Please login!',
    'Password confirm error!':'Password confirm error!',
    'userName_required':'Username is required!',
    'password_required':'Password is required!',
    'userName_invalid':'User name must be combine with 6 to 16 letters, Numbers, or underscores!',
    'password_invalid':'Password must be combine with 6 to 16 letters, Numbers, or underscores!'
  }
  window.lanCn = {
    'Network error!Please retry!':'网络异常！请重试！',
    'RECORD LOGIN':'录音登录',
    'Username':'用户名',
    'Password':'密码',
    'Remember me':'记住我',
    'LOGIN':'登录',
    'User Alias':'别名',
    'Confirm Password':'确认密码',
    'Register success!Please login!':'注册成功！请返回登录！',
    'Password confirm error!':'两次密码输入不正确！',
    'userName_required':'请输入用户名！',
    'password_required':'请输入密码！',
    'userName_invalid':'用户名必须为长度6-16的字母、数字或下划线组合！',
    'password_invalid':'密码必须为长度6-16的字母、数字或下划线组合！'
  }
  window.lan = lan;
  window.getLanValue = function(value){
    if(window.lan == 'en'){
      return window.lanEn[value];
    }else{
      return window.lanCn[value];
    }
  }
  var rcUserName = $.cookie('rcUserName');
  var rcPassword = $.cookie('rcPassword');
  if(rcUserName && rcPassword){
    setTimeout(function(){
      $('#rcPassword').val(base64_decode(rcPassword));
      $('#rcUserName').val(rcUserName);
    },100)
  }
  //获取鼠标焦点
  $('#loginForm input[name=rcUserName]').focus();

  $('#login').bind('keydown',function(e){
    // 兼容FF和IE和Opera
    var theEvent = e || window.event;
    var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
      //回车执行查询
      doLogin();
    }
  })

  //默认使用中文
  changeLan('cn')
}

function changeLan(lan){
  window.lan = lan;
  $('.Login-head h3').html(window.getLanValue('RECORD LOGIN'));
  $('.ticker h4').html(window.getLanValue('Username'));
  $('#rcUserName').attr('placeholder',window.getLanValue('Username'));
  $('#passwordField h4').html(window.getLanValue('Password'));
  $('#rcPassword').attr('placeholder',window.getLanValue('Password'));
  $('.radio').html('<input type="checkbox" id="radio-inline" name="radio-inline" style="height:15px;width:15px">' + window.getLanValue('Remember me'));
  $('.submit-button input').attr('value',window.getLanValue('LOGIN'));
}

function doLogin(){
  //表单验证
  if(!$('#rcUserName').val()){
    $('#errMsg').html(window.getLanValue('userName_required'))
    return;
  }
  if(!$('#rcPassword').val()){
    $('#errMsg').html(window.getLanValue('password_required'))
    return;
  }

  var rcUserName = $('#rcUserName').val();
  var rcPassword = base64_encode($('#rcPassword').val());
  var dataLan = "";
  //清空errMsg
  $('#errMsg').html('');

  //判断并保存cookies
  if($('#radio-inline').prop('checked')){
    $.cookie('rcUserName', rcUserName, { expires: 7 });
    $.cookie('rcPassword', rcPassword, { expires: 7 });
  }

  $.cookie('userLan', window.lan, { expires: 7 });
  if(window.lan == 'cn'){
    dataLan = 'zh_CN';
  }else{
    dataLan = 'en_US';
  }
  $.ajax({
    url:'/api',
    data:{action:'loginAction',rcUserName:rcUserName,rcPassword:rcPassword,localLan:dataLan},
    type:'POST',
    complete:function(data,str){
      if(data && data.status == 200){
        if(data.responseJSON && data.responseJSON.success){
          window.location.href = 'bootstrap';
        }else{
          if(typeof(data.responseJSON.errMsg) == 'object'){
            var msg = '';
            for(var i in data.responseJSON.errMsg){
              if(data.responseJSON.errMsg[i][0]){
                msg += data.responseJSON.errMsg[i] + '!'
              }else{
                continue;
              }
            }
            $('#errMsg').html(msg);
          }else{
            $('#errMsg').html(data.responseJSON.errMsg).css('color','red');
          }
        }
      }else{
        $('#errMsg').html(window.getLanValue('Network error!Please retry!')).css('color','red');
      }
    }
  })
}

