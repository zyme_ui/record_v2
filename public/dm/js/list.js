define([],function (){
	function changeView(pid,cid,changeWidth){
		var width=$(window).width();
		var maxWidth=600;
		if(changeWidth){
			maxWidth=changeWidth;
		}
		var ns=$('#'+cid).find("div[class=card-view]");
		var bt=$('#'+pid).find("button[name=toggle]");
		if(bt.length){
			//小于600宽切换成名片布局,反之切换成列表布局
			if(width<=maxWidth){				
				if(ns.length){
					//当前已经是名片布局,什么都不做
				}else{
					bt.trigger("click");
				}
			}else{
				if(ns.length){					
					bt.trigger("click");
				}else{
					//当前已经是列表布局,什么都不做
				}
			}
		}
		$('#'+cid).bootstrapTable('resetView');
	}	
	function changeForAce(id){
	    //ace框架与bootstrap-table冲突，需要修改
	    var db=$('#'+id).find(".btn-default");
	    if(db.length){
		    db.each(function(){
		    	$(this).removeClass("btn-default");
		    	$(this).addClass("btn-sm");
		    	$(this).addClass("btn-info");
		    })
	    }
	    var s=$('#'+id).find(".search input");
	    if(s.length){
	    	s.addClass("input-sm");
	    }
	    var p=$('#'+id).find(".page-size button");
	    console.log(p)
	    if(p.length){
	    	p.removeClass("btn-default");
	    	p.addClass("btn-info");
	    	p.addClass("btn-sm");
	    }
	}
    return {
    	changeView:changeView,
    	changeForAce:changeForAce
    };
});


