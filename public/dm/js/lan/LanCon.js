define(function (){
	function changeLan(value){
		//
		setCookie("userLan",value, "one month");
		window.location.reload();
	}
	function initEvent(){
		$("#lancon li a").bind("click",function(){
			var val=$(this).attr("val");
			$("#lant").html('<i class="fa fa-language"></i>&nbsp;'+$(this).html()+' <b class="caret"></b>');
			if(val=="cn"){				
				setCookie("userLan",1, "one month");
				window.location.reload();
			}else{
				setCookie("userLan",2, "one month");
				window.location.reload();
			}
		});
	}
	function getTableList(){
		return [{name:"dev",value:window.lan["dev"]},{name:"domain",value:window.lan["domain"]},{name:"zone",value:window.lan["zone"]}
		,{name:"site",value:window.lan["site"]}];
	}
	function getWordList(tb){
		var ret=[{name:"name",value:window.lan["name"]},{name:"alias",value:window.lan["alias"]},{name:"desc",value:window.lan["desc"]}];
		if(tb==window.lan["dev"]){
			ret=[{name:"alias",value:window.lan["alias"]},{name:"desc",value:window.lan["desc"]}
			,{name:"version",value:window.lan["version"]}
			,{name:"productName",value:window.lan["productName"]}];
		}
		return ret;
	}
	function parseValues(values){
		console.log(values);
		var t=values.split(",");
		var tl=getTableList();
		var ret="";
		for(var i=0;i<t.length;i++){
			if(ret!=""){
				ret=ret+",";
			}
			var tt=t[i].split("*");
			
			
			
			for(var k=0;k<tl.length;k++){
				if(tl[k].value==tt[0]){
					ret+=tl[k].name;
					ret=ret+"*";
					break;
				}
			}
			var wl=getWordList(tt[0]);
			
			for(var j=0;j<wl.length;j++){
				if(wl[j].value==tt[1]){
					ret+=wl[j].name;
					ret=ret+"*";
					break;
				}
			}
			ret+=tt[2];
		}
		
		console.log(ret);
		return ret;
	}
	function initLan(callAfterLan){
		initEvent();
		var c = getCookie("userLan");
    if(c == 'cn'){
      c = 1;
    }
		if(!c){
			require(["lan-cn"], function(cn) { 
				window.lan=cn;
				$("#lant").html('<i class="fa fa-language"></i>&nbsp;'+$("#lancn").html()+' <b class="caret"></b>');
				if(callAfterLan) callAfterLan();
			});	
		}else{
			if(c==1){
				require(["lan-cn"], function(cn) { 
					window.lan=cn;
					$("#lant").html('<i class="fa fa-language"></i>&nbsp;'+$("#lancn").html()+' <b class="caret"></b>');
					if(callAfterLan) callAfterLan();
				});
			}else{
				require(["lan-en"], function(en) { 
					window.lan=en;
					$("#lant").html('<i class="fa fa-language"></i>&nbsp;'+$("#lanen").html()+' <b class="caret"></b>');
					if(callAfterLan) callAfterLan();
				});
			}
		}
	}
	function isEn(){
		var c = getCookie("userLan");
		if(c==1){
			return false;
		}
		return true;
	}
	function getValue(name,val){
		var lan=window.lan;
		if(!lan) return;
		var c = getCookie("userLan");
		var str=name;
		if(val!=undefined && val!=null){
			str=str+"_"+val;
		}
		if(lan[str]){
			return lan[str];
		}else if(c==1){
			return "未知";
		}else{
			return "Unknow";
		}		
	}
	function getProductType(productId){
		if(productId==17 || productId==18 || productId==19 || productId==11 || productId==12 || productId==15 || productId==16){
			return "DAG";
		}else if(productId==1 || productId==2 || productId==3 || productId==4){
			return "MTG";
		}
		return productId;
	}
	function getCookie(name)//取cookies函数
	{
		var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
		if(arr != null) return unescape(arr[2]); return null;

	}

	function setCookie(name,value, time)//两个参数，一个是cookie的名子，一个是值
	{
		var exp = new Date(); //new Date("December 31, 9998");
		var Days;//cookie 保留的天数
		var month;
		var year;
		if(time == 0)
		{
			Days = 0;
		}
		else if (time=="one day")
		{
			Days = 1;
		}
		else if (time=="one month")
		{
			month = exp.getMonth()+1;
			year =  exp.getFullYear();
			Days = GetDayNum(month,year);
		}
		else if(time=="one year")
		{
			year =  exp.getFullYear();
			if(IsLeapYear(year))
			{
				Days = 366;
			}
			else
			{
				Days = 365;
			}
		}

		exp.setTime(exp.getTime() + Days*24*60*60*1000);
		document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
	}
	function GetDayNum(month,year)
	{
		if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
		{
			return 31;
		}
		else if(month==4 || month==6 || month==9 || month==11)
		{
			return 30;
		}
		else if(month == 2)
		{
			if(IsLeapYear(year))
			{
				return 29;
			}
			else
			{
				return 28;
			}
		}
		else
		{
			return 0;
		}
	}

	function IsLeapYear(iYear) 
	{
		if (iYear % 4 == 0 && iYear % 100 != 0)
		{
			return true;
		} 
		else if(iYear % 400 == 0) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}
	return {
		getValue:getValue,
		setCookie:setCookie,
		getCookie:getCookie,
		initLan:initLan,
		parseValues:parseValues,
		getTableList:getTableList,
		getWordList:getWordList,
		getProductType:getProductType,
		isEn:isEn
	};
});


