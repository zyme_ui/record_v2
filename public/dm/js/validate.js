/**
 * Created by Rainc on 2015/6/3.
 */

$.validator.addMethod("digits", function(value, element) {
  return /^[0-9]*$/.test(value);
});
$.validator.addMethod("ip", function(value, element) {
  return /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/.test(value);
});
$.validator.addMethod("password", function(value, element) {
  return /^[A-Za-z0-9\_]{5,16}$/.test(value);
});
$.validator.addMethod("userName", function(value, element) {
  return /^[A-Za-z0-9\_]{5,16}$/.test(value);
});
$.validator.addMethod("mode", function(value, element) {
  return /^[Xx0-9\+\[\]\{\}\,]{1}[Xx0-9\+\-\[\]\{\}\,]*$/.test(value);
});
$.validator.addMethod("sn", function(value, element) {
  return /^([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})\-([A-Fa-f0-9]{4})$/.test(value);
});
$.validator.addMethod("domainName", function(value, element) {
  return /^[0-9a-zA-Z]+[0-9a-zA-Z\.-]*\.[a-zA-Z]{2,4}$/.test(value);
});
$.validator.addMethod("cloudDomainName", function(value, element) {
  return /^([0-9a-zA-Z])[0-9a-zA-Z\.-]*([0-9a-zA-Z])$/.test(value);
});
$.validator.addMethod("connectToCloud", function(value, element) {
  return /^(true|false)$/.test(value);
});
//域名或ip
$.validator.addMethod("domainNameOrIp", function(value, element) {
  if(/^[0-9a-zA-Z]+[0-9a-zA-Z\.-]*\.[a-zA-Z]{2,4}$/.test(value) ||
    /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/.test(value)){
    return true;
  }else{
    return false;
  }
});
//路径判断
$.validator.addMethod("urlPath", function(value, element) {
  return /^(\/[\w\-\.\/?%&=]*)+$/.test(value);
});