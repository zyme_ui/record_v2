define(["form-field","base64"],function (field,bs64){
  function createPanel(userId){
		var pn=$("#myModal");
		if(!pn) return;
		var html='<div class="modal-dialog">'
		      +'<div class="modal-content">'
		      +'<div class="modal-header">'
		      +'<button type="button" class="close" '
		      +'data-dismiss="modal" aria-hidden="true">'
		      +'&times;'
		      +'</button>'
		      +'<h4 class="modal-title" id="myModalLabel">'
		      +window.lan['changePwd']
		      +'</h4>'
		      +'</div>'
		      +'<div class="modal-body">'
		      +'<form id="passwordForm" class="" role="form">'
		      +'<div class="row">'
		      +'<div class="col-md-12" >';
		html+=field.getTextField("oldPwd","",window.lan['old password'],"","password")
		html+=field.getTextField("newPwd","",window.lan['new password'],"","password")
		html+=field.getTextField("confirmPwd","",window.lan['confirm password'],"","password")
		      html+='</div>'			  
		      +'</div>'
		      +'</form>'
		      +'</div>'
		      +'<div class="modal-footer">'
		      +'<button name="close" type="button" class="btn btn-default" '
		      +'data-dismiss="modal">' + window.lan['close']
		      +'</button>'
		      +'<button name="commit" type="button" class="btn btn-primary">'
		      +window.lan['submit']
		      +'</button>'
		      +'</div>'
		      +'</div><!-- /.modal-content -->'
		      +'</div>';
		pn.html("");
		pn.append(html);
		$('#myModal').modal().css({
		    width: 'auto',
		    backdrop:false
		});
		$('#myModal button[name=commit]').bind("click",function(){
			var oldPwd=$("#myModal input[name=oldPwd]").val();
			var newPwd=$("#myModal input[name=newPwd]").val();
			var confirmPwd=$("#myModal input[name=confirmPwd]").val();
			$('#passwordForm').validate({
				rules: {
					oldPwd:{
						required: true,
						password: true
					},
					newPwd:{
						required: true,
						password: true
					},
					confirmPwd:{
						required: true,
						password: true
					}
				},
				messages: {
					oldPwd: {
						required: window.lan['Old password must not be empty!'],
						password: window.lan['Password must be 5-16 combination of letters, Numbers, or underscores!']
					},
					newPwd:{
						required: window.lan['New password must not be empty!'],
						password: window.lan['Password must be 5-16 combination of letters, Numbers, or underscores!']
					},
					confirmPwd:{
						required: window.lan['Confirm password must not be empty!'],
						password: window.lan['Password must be 5-16 combination of letters, Numbers, or underscores!']
					}
				}
			});
			if(confirmPwd!=newPwd){
				window.tip.show_pk("warning",null,window.lan['New password must be the same as Confirm password!']);
				return;
			}
			if($('#passwordForm').valid()){
				$.ajax({
					url: "/api",
					type: 'Post',
					data: {action: 'changePwd', oldPwd: bs64.base64_encode(oldPwd), newPwd: bs64.base64_encode(newPwd)},
					complete: function (data, str) {
						if (data.responseJSON && data.responseJSON.success) {
							$('#myModal button[name=close]').trigger("click");
							window.tip.show_pk("success", null, window.lan['Change password success!']);
						} else {
							window.tip.show_pk("danger", null, window.lan['Change password failed!'] +  data.responseJSON.errMsg);
						}
					}
				})
			}
		});
  }

  return {
	  createPanel:createPanel
  };
});


