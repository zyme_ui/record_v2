/**
 * Created by Rainc on 2015/9/21.
 */

define(["form-field","rc-fun","bootstrap-table"],function(field,rf){
  function createRecordInfo(pid,id){
    var pn=$("#"+pid);
    if(!pn){
      return;
    }
    var html = '<div id="'+id+'-toolbar" ><div class="btn-group" role="group" aria-label="...">'
    + '<button id="'+id+'-del" class="btn btn-danger btn-sm" title="' + window.lan['delete'] + '"><i class="fa fa-minus-square bigger-100"></i>&nbsp;' + window.lan['delete'] + '</button>'
    + '<button id="'+id+'-empty" class="btn btn-danger btn-sm" title="' + window.lan['empty'] + '"><i class="fa fa-trash bigger-100"></i>&nbsp;' + window.lan['empty'] + '</button>'
    + '</div></div>';
    html+='<table id='+id+'></table>';
    pn.html("");
    pn.append(html);

    createColEvents(pid,id);

    $('#'+id).bootstrapTable({
      method: 'post',
      contentType:'application/x-www-form-urlencoded;charset=UTF-8',
      url: '/api',
      cache: false,
      queryParams:function(p){
        p.action = 'getRecordList';
        return p;
      },
      height:515,
      striped: true,
      pagination: true,
      toolbar:"#"+id+"-toolbar",
      pageSize: 10,
      pageList: [10, 50,200,500,1000,'All'],
      search: false,
      showColumns: true,
      showRefresh: true,
      showToggle:true,
      smartDisplay:true,
      minimumCountColumns: 2,
      undefinedText: '-',
      clickToSelect:true,
      sortName: 'fileName',
      columns: [{
        checkbox: true,
        valign: 'middle'
      },{
        field: 'fileName',
        title: window.lan['fileDir'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: '',
        title: '',
        align: 'center',
        valign: 'middle',
        sortable: false,
        clickToSelect: true,
        formatter:function(value,row,index){
          var html='';
          html += '<a action="check" class="blue" href="#" title="' + window.lan['check'] + '">'
          +'<i class="fa fa-search bigger-130"></i>'
          +'</a>'
          +'&nbsp;';
          return html;
        },
        events:operateEvents
      }]
    });
    window.list.changeForAce(pid);
    $(window).resize(function () {
      window.list.changeView(pid,id,600);
    });
    window.list.changeView(pid,id,600);
    $("#"+ id +"-del").click(function(e){
      delItem(id);
    })
    $("#"+ id +"-empty").click(function(e){
      emptyItem(id);
    })
  }
  function createColEvents(pid,id){
    var check=function (e, value, row, index) {
      checkItem(pid,id,[row]);
    };
    window.operateEvents = {
      'click a[action=check]':check
    };
  }
  function checkItem(pid,id,row){
    require(['rc-infoDetail'],function(ri){
      ri.createCallInfoDetail(pid,id,row[0].fileName);
    })
  }
  function delItem(tid){
    var rows=$('#'+tid).bootstrapTable('getSelections');
    if(rows.length==0) {
      window.tip.show_pk("warning", null, window.lan['You must select a item!']);
      return;
    }else {
      var items = [];
      for (var i = 0; i < rows.length; i++) {
        items.push(rows[i].fileName);
      }
      rf.createDelItemConfirm(function(){
        $('#myModal').modal().css({
          width: 'auto',
          backdrop:false
        });
        $('#myModal button[name=commit]').bind("click",function(){
          $('#myModal button[name=close]').trigger("click");
          window.tip.show_pk("info",60,window.lan['Deleting items...'],true);
          $.ajax({
            url: "/api",
            traditional : true,
            type: 'post',
            data:{action:'deleteRecordDir',items:items},
            complete: function(data,str){
              if(data.status == 200){
                if(data.responseJSON && data.responseJSON.success){
                  $('#'+tid).bootstrapTable('refresh');
                  window.tip.show_pk("success",null,window.lan['Delete items success!']);
                }else{
                  window.tip.show_pk("danger",null,window.lan['Delete items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
                }
              }else{
                window.tip.show_pk("danger",null,window.lan['Network error!']);
              }
            }});
        });
      })
    }
  }
  function emptyItem(tid){
    rf.createEmptyItemConfirm(function(){
      $('#myModal').modal().css({
        width: 'auto',
        backdrop:false
      });
      $('#myModal button[name=commit]').bind("click",function(){
        $('#myModal button[name=close]').trigger("click");
        window.tip.show_pk("info",60,window.lan['Emptying items...'],true);
        $.ajax({
          url: "/api",
          traditional : true,
          type: 'post',
          data:{action:'emptyRecordDir'},
          complete: function(data,str){
            if(data.status == 200){
              if(data.responseJSON && data.responseJSON.success){
                $('#'+tid).bootstrapTable('refresh');   //ˢ���б�
                window.tip.show_pk("success",null,window.lan['Empty items success!']);
              }else{
                window.tip.show_pk("danger",null,window.lan['Empty items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            }else{
              window.tip.show_pk("danger",null,window.lan['Network error!']);
            }
          }});
      });
    })
  }
  return{
    createRecordInfo:createRecordInfo
  }
})