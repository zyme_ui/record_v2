/**
 * Created by Rainc on 2015/9/21.
 */
define(['rc-fun','rc-main','bootstrap-table'],function(rf,rm){
  function createAlarmLogList(pid,id){
    var pn=$("#"+pid);
    if(!pn){
      return;
    }
    var html =  '<div id="'+id+'-toolbar" class="btn-group" role="group" aria-label="...">';
    html+= '<button id="'+id+'-confirmAlarm" class="btn btn-success btn-sm" title="' + window.lan['confirm alarm'] + '"><i class="fa fa-check-square bigger-100"></i>&nbsp;' + window.lan['confirm alarm'] + '</button>'
    + '<button id="'+id+'-delAlarm" class="btn btn-danger btn-sm" title="' + window.lan['delete alarm'] + '"><i class="fa fa-minus-square bigger-100"></i>&nbsp;' + window.lan['delete alarm'] + '</button>'
    + '<button id="'+id+'-emptyAlarm" class="btn btn-danger btn-sm" title="' + window.lan['empty alarm'] + '"><i class="fa fa-trash bigger-100"></i>&nbsp;' + window.lan['empty alarm'] + '</button>'
    html+='</div>'
    html+='<table id='+id+'></table>';
    pn.html("");
    pn.append(html);

    $('#'+id).bootstrapTable({
      method: 'post',
      contentType:'application/x-www-form-urlencoded;charset=UTF-8',
      url: '/api',
      cache: false,
      responseHandler:function(res){
        if(res.success) {
          if(res['result']["total"] != 0 && res.result.list.length == 0){
            $('#' +id).bootstrapTable('selectPage',1);
            return {rows:[],total:0}
          }else{
            var obj={};
            if(res && res.result.list && res.result.list.length>0){
              obj["rows"]=res.result.list;
              obj.total=res['result']["total"];
              return obj;
            }else{
              obj["rows"]= [];
              obj.total=res['result']["total"];
              return obj;
            }
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['get alarm log failed!'] + window.format.getErrmsg(res.errMsg));
          if(res.errMsg == 'Session timeout!Please relogin!'){
            setTimeout(function(){
              window.location.reload();
            },2000)
          }
        }
      },
      queryParams:function(p){
        p.action = 'getAlarmLog';
        return p;
      },
      height:515,
      striped: true,
      pagination: true,
      toolbar:"#"+id+"-toolbar",
      pageSize: 10,
      pageList: [10, 50,200,500,1000,'All'],
      search: true,
      showColumns: true,
      showRefresh: true,
      showToggle:true,
      queryParamsType:'limit',
      sidePagination: "server",
      smartDisplay:true,
      minimumCountColumns: 2,
      undefinedText: '-',
      clickToSelect:true,
      columns: [{
        checkbox: true,
        valign: 'middle'
      },{
        field: 'uuid',
        title: 'id',
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'level',
        title: window.lan['level'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible:true,
        formatter:function(value,row,index){
          if(value == 1){
            return '<div style="background-color: green;color:white">' + window.lan['alarmLevel_debug'] + '</div>'
          }else if(value == 2){
            return '<div style="background-color: skyblue;color:white">' + window.lan['alarmLevel_notice'] + '</div>'
          }else if(value == 3){
            return '<div style="background-color: orange;color:white">' + window.lan['alarmLevel_warning']+ '</div>'
          }else if(value == 4){
            return '<div style="background-color: orangered;color:white">' + window.lan['alarmLevel_error'] + '</div>'
          }else if(value == 5){
            return '<div style="background-color: red;color:white">' + window.lan['alarmLevel_serious'] + '</div>'
          }
          return '---';
        }
      },{
        field: 'alarm_object',
        title: window.lan['alarm object'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value == 0){
            return window.lan['unknown'];
          }else if(value == 1){
            return window.lan['database'];
          }else if(value ==2){
            return window.lan['cloud'];
          }else if(value ==3){
            return window.lan['system service'];
          }else if(value ==4){
            return window.lan['cpu userate'];
          }else if(value ==5){
            return window.lan['server memory userate'];
          }else if(value ==6){
            return window.lan['disk userate'];
          }
          return "---";
        }
      },{
        field: 'alarm_cause',
        title: window.lan['alarm cause'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true,
        formatter:function(value,row,index){
          if(value == 'CPU_ALARM'){
            return window.lan['CPU userate alarm'];
          }else if(value == 'SERVER_MEMORY_ALARM'){
            return window.lan['server memory userate alarm'];
          }else if(value == 'SYSTEM_ALARM'){
            return window.lan['system service alarm'];
          }else if(value == 'SERVER_DISK_ALARM'){
            return window.lan['server disk userate alarm'];
          }else if(value == 'DATABASE_ALARM'){
            return window.lan['database alarm'];
          }else if(value == 'CLOUD_ALARM'){
            return window.lan['cloud alarm'];
          }
          return "---";
        }
      },{
        field: 'alarm_detail',
        title: window.lan['alarm detail'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true,
        formatter:function(value,row,index){
          if(value){
            return value;
          }
          return "---";
        }
      },{
        field: 'confirmed',
        title: window.lan['ever confirmed'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value == 1){
            return '<span style="color: red">' + window.lan['Unconfirmed'] + '</span>';
          }else if(value ==2){
            return '<span style="color: green">' + window.lan['confirmed'] + '</span>';
          }
          return "---";
        }
      },{
        field: 'confirm_time',
        title: window.lan['confirm time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'create_time',
        title: window.lan['alarm create time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      }]
    });
    window.list.changeForAce(pid);
    $(window).resize(function () {
      window.list.changeView(pid,id,600);
    });
    window.list.changeView(pid,id,600);
    $("#"+ id +"-confirmAlarm").click(function(e){
      confirmAlarm(id);      //确认告警
    })
    $("#"+ id +"-delAlarm").click(function(e){
      delAlarm(id);      //删除告警
    })
    $("#"+ id +"-emptyAlarm").click(function(e){
      emptyAlarm(id);      //清空
    })
  }
  function confirmAlarm(tid){
    var rows=$('#'+tid).bootstrapTable('getSelections');
    if(rows.length==0) {
      window.tip.show_pk("warning", null, window.lan['You must select a item!']);
      return;
    }else {
      var alarms = [];
      for (var i = 0; i < rows.length; i++) {
        alarms.push(rows[i].uuid);
      }
      rf.createConfirmAlarmConfirm(function(){
        $('#myModal').modal().css({
          width: 'auto',
          backdrop:false
        });
        $('#myModal button[name=commit]').bind("click",function(){
          $('#myModal button[name=close]').trigger("click");
          window.tip.show_pk("info",60,window.lan['Confirming alarms...'],true);
          $.ajax({
            url: "/api",
            traditional : true,
            type: 'post',
            data:{action:'confirmAlarms',alarms:alarms},
            complete: function(data,str){
              rm.checkSeriousExist();    //检测严重告警是否存在后改变tab闪烁样式
              if(data.status == 200){
                if(data.responseJSON && data.responseJSON.success){
                  $('#'+tid).bootstrapTable('refresh');   //刷新列表
                  window.tip.show_pk("success",null,window.lan['Confirm alarms success!']);
                }else{
                  window.tip.show_pk("danger",null,window.lan['Confirm alarms failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
                }
              }else{
                window.tip.show_pk("danger",null,window.lan['Network error!']);
              }
            }});
        });
      })
    }
  }
  function delAlarm(tid){
    var rows=$('#'+tid).bootstrapTable('getSelections');
    if(rows.length==0) {
      window.tip.show_pk("warning", null, window.lan['You must select a item!']);
      return;
    }else {
      var alarms = [];
      for (var i = 0; i < rows.length; i++) {
        alarms.push(rows[i].uuid);
      }
      rf.createDelAlarmConfirm(function(){
        $('#myModal').modal().css({
          width: 'auto',
          backdrop:false
        });
        $('#myModal button[name=commit]').bind("click",function(){
          $('#myModal button[name=close]').trigger("click");
          window.tip.show_pk("info",60,window.lan['Deleting alarms...'],true);
          $.ajax({
            url: "/api",
            traditional : true,
            type: 'post',
            data:{action:'deleteAlarms',alarms:alarms},
            complete: function(data,str){
              if(data.status == 200){
                if(data.responseJSON && data.responseJSON.success){
                  $('#'+tid).bootstrapTable('selectPage','1');   //跳到第一页
                  window.tip.show_pk("success",null,window.lan['Delete alarms success!']);
                }else{
                  window.tip.show_pk("danger",null,window.lan['Delete alarms failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
                }
              }else{
                window.tip.show_pk("danger",null,window.lan['Network error!']);
              }
            }});
        });
      })
    }
  }
  function emptyAlarm(tid){
    rf.createEmptyAlarmConfirm(function(){
      $('#myModal').modal().css({
        width: 'auto',
        backdrop:false
      });
      $('#myModal button[name=commit]').bind("click",function(){
        $('#myModal button[name=close]').trigger("click");
        window.tip.show_pk("info",60,window.lan['Emptying alarms...'],true);
        $.ajax({
          url: "/api",
          traditional : true,
          type: 'post',
          data:{action:'emptyAlarms'},
          complete: function(data,str){
            if(data.status == 200){
              if(data.responseJSON && data.responseJSON.success){
                $('#'+tid).bootstrapTable('refresh');   //刷新列表
                window.tip.show_pk("success",null,window.lan['Empty alarms success!']);
              }else{
                window.tip.show_pk("danger",null,window.lan['Empty alarms failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            }else{
              window.tip.show_pk("danger",null,window.lan['Network error!']);
            }
          }});
      });
    })
  }
  return{
    createAlarmLogList:createAlarmLogList
  }
})
