/**
 * Created by Rainc on 2015/9/21.
 */
define(["form-field","rc-fun","tPlayer","bootstrap-table"],function(field,rf,tp){
  function createCallInfoDetail(pid,id,dirName){
    var pn=$("#"+pid);
    if(!pn){
      return;
    }
    var html =  field.getSellectText(id + '_label_name','1',[{value:0,text:''},{value:1,text:window.lan['MTG_ADDRESS']}
      ,{value:2,text:window.lan['caller']},{value:3,text:window.lan['called']}],id+ '_select_name','1',[{value:0,text:''}
      ,{value:1,text:'='},{value:2,text:window.lan['like']}],id + '_text_name','','',['col-md-6']);
    html += '<div id="'+id+'-toolbar" ><div class="btn-group" role="group" aria-label="...">'
    + '<button id="'+id+'-back" class="btn btn-success btn-sm" title="' + window.lan['back'] + '"><i class="fa fa-chevron-circle-left  bigger-100"></i>&nbsp;' + window.lan['back'] + '</button>'
    + '<button id="'+id+'-queryCurrent" class="btn btn-success btn-sm" title="' + window.lan['queryCurrent'] + '"><i class="fa fa-search bigger-100"></i>&nbsp;' + window.lan['queryCurrent'] + '</button>'
    + '<button id="'+id+'-queryGlobal" class="btn btn-success btn-sm" title="' + window.lan['queryGlobal'] + '"><i class="fa fa-search bigger-100"></i>&nbsp;' + window.lan['queryGlobal'] + '</button>'
    + '<button id="'+id+'-cancelQuery" class="btn btn-success btn-sm" title="' + window.lan['cancel query'] + '"><i class="fa fa-eraser bigger-100"></i>&nbsp;' + window.lan['cancel query'] + '</button>'
    + '<button id="'+id+'-del" class="btn btn-danger btn-sm" title="' + window.lan['delete'] + '"><i class="fa fa-minus-square bigger-100"></i>&nbsp;' + window.lan['delete'] + '</button>'
    + '<button id="'+id+'-empty" class="btn btn-danger btn-sm" title="' + window.lan['empty'] + '"><i class="fa fa-trash bigger-100"></i>&nbsp;' + window.lan['empty'] + '</button>'
    + '</div></div>';
    html+='<table id='+id+'></table>';
    pn.html("");
    pn.append(html);

    createColEvents(pid,id);

    $('#'+id).bootstrapTable({
      method: 'post',
      contentType:'application/x-www-form-urlencoded;charset=UTF-8',
      url: '/api',
      cache: false,
      queryParams:function(p){
        p.dir = dirName;
        p.action = 'getRecordList';
        return p;
      },
      height:515,
      striped: true,
      pagination: true,
      toolbar:"#"+id+"-toolbar",
      pageSize: 10,
      pageList: [10, 50,200,500,1000,'All'],
      search: false,
      showColumns: true,
      showRefresh: true,
      showToggle:true,
      smartDisplay:true,
      minimumCountColumns: 2,
      undefinedText: '-',
      clickToSelect:true,
      sortName: 'fileCtime',
      sortOrder: 'desc',
      columns: [{
        checkbox: true,
        valign: 'middle'
      },{
        field: 'fileName',
        title: window.lan['fileName'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'MTG_ADDRESS',
        title: window.lan['MTG_ADDRESS'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: true
      },{
        field: 'caller',
        title: window.lan['caller'],
        align: 'center',
        valign: 'middle',
        sortable: false
      },{
        field: 'called',
        title: window.lan['called'],
        align: 'center',
        valign: 'middle',
        sortable: false
      },{
        field: 'filePath',
        title: window.lan['filePath'],
        align: 'center',
        valign: 'middle',
        sortable: false
      },{
        field: 'parentFileName',
        title: window.lan['parentFileName'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'fileCtime',
        title: window.lan['fileCtime'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value && value!= '0000-00-00 00:00:00'){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      },{
        field: 'fileSize',
        title: window.lan['fileSize'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: '',
        title: '',
        align: 'left',
        valign: 'middle',
        sortable: false,
        clickToSelect: true,
        formatter:function(value,row,index){
          var html='';
          html += '<a action="play" class="blue" href="#" title="' + window.lan['play'] + '">'
          +'<i class="fa fa-play-circle bigger-130"></i>'
          +'</a>'
          +'&nbsp;';
          html += '<a action="download" class="blue" href="#" title="' + window.lan['download'] + '">'
          +'<i class="fa fa-download bigger-130"></i>'
          +'</a>'
          +'&nbsp;';
          return html;
        },
        events:operateEvents
      }]
    });
    window.list.changeForAce(pid);
    $(window).resize(function () {
      window.list.changeView(pid,id,600);
    });
    window.list.changeView(pid,id,600);
    $("#"+ id +"-del").click(function(e){
      delItem(id,dirName);
    })
    $("#"+ id +"-empty").click(function(e){
      emptyItem(id,dirName);
    })
    $("#"+ id +"-queryCurrent").click(function(e){
      searchRecord(id,dirName);
    })
    $("#"+ id +"-queryGlobal").click(function(e){
      searchRecord(id,'');
    })
    $("#"+ id +"-back").click(function(e){
      backforward(pid,id);
    })
    $("#"+ id +"-cancelQuery").click(function(e){
      cancelSearch(id,dirName);
    })
    //绑定enter事件
    $('input[name=' + id + '_text_name]').bind('keyup',function(e){
      e.preventDefault();
      if (e.keyCode == "13") {
        //回车执行查询
        searchRecord(id,dirName);
      }
    })
  }
  function createColEvents(pid,id){
    var play=function (e, value, row, index) {
      playItem(id,[row]);
    };
    var download=function (e, value, row, index) {
      downloadItem(id,[row]);
    };
    window.operateEvents = {
      'click a[action=play]':play,
      'click a[action=download]':download
    };
  }
  function downloadItem(id,row){
    var fileName = row[0].fileName;
    var out_format = 'wav';
    var fileArr = fileName.split('.')[0].split('_');
    fileArr.pop();
    fileName = fileArr.join('_');
    var url = '/api?action=download&out_format=' + out_format + '&filePath=' + row[0]['filePath'] + "&fileName=" + fileName;
    window.open(url,row[0].fileName);
  }
  function playItem(id,row){
    var fileName = row[0].fileName;
    var out_format = 'wav';
    var fileArr = fileName.split('.')[0].split('_');
    fileArr.pop();
    fileName = fileArr.join('_');
    //$.ajax({
    //  url: "/api",
    //  traditional : true,
    //  type: 'post',
    //  data:{action:'playRecordItem',item:item},
    //  complete: function(data,str){
    //    if(data.status == 200){
    //      if(data.responseJSON && data.responseJSON.success){
    //        $('#'+tid).bootstrapTable('refresh');
    //        window.tip.show_pk("success",null,window.lan['Delete items success!']);
    //      }else{
    //        window.tip.show_pk("danger",null,window.lan['Delete items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
    //      }
    //    }else{
    //      window.tip.show_pk("danger",null,window.lan['Network error!']);
    //    }
    //  }});
    //播放录音文件
    //构建ul列表的html
    var html = '';
    html += '<li t_cover="goldfrapp.jpg" t_artist="" t_name=""><a href="#">' + row[0].fileName + '</a>' +
    '<audio preload="auto"><source src="/api?action=playRecordItem&fileName=' + fileName + '&out_format=' + out_format
    + '&parentFileName=' + row[0].parentFileName + '" type="audio/ogg">' +
    '</audio></li>'
    $('#playlist ul').html(html);
    //初始化录音播放器
    if(!window.inited){
      tp.init();
    }
    tp.initPlaylist();
    //当播放器隐藏时，显示播放器并改变css
    if($('#media').css('display') == 'none'){
      $('#media').fadeIn('fast');
      $('#my-tab-position').css('margin-top','-30px')
    }
    //默认点击播放
    $('#playlist ul li:first a').trigger('click');
  }
  function backforward(pid,id){
    require(['rc-info'],function(ri){
      ri.createRecordInfo(pid,id)
    })
  }
  function searchRecord(tid,dirName){
    var labelValue = $('select[name=' + tid + '_label_name]').val();
    var selectValue = $('select[name=' + tid + '_select_name]').val();
    var textValue = $('input[name=' + tid + '_text_name]').val();
    var dataValue = {action:'searchRecord',dirName:dirName};
    if(labelValue != 0 && selectValue != 0 && textValue!= ''){
      dataValue['labelValue'] = labelValue;
      dataValue['selectValue'] = selectValue;
      dataValue['textValue'] = textValue;
    }
    $.ajax({
      url: "/api",
      data: dataValue,
      type:"post",
      complete: function(data,str){
        if(data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            window.tip.show_pk("success",null,window.lan['Query record info success!'] );
            $('#'+tid).bootstrapTable('load',data.responseJSON.result.list);
          }else{
            window.tip.show_pk("danger",null,window.lan['Query record info failed!'] +  window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }});
  }
  function cancelSearch(tid,dirName){
    //将选项重置
    $('input[name=' + tid + '_text_name]').val('');
    var dataValue = {action:'getRecordList',order:'desc',limit:'10',offset:'0',dir:dirName,sort:'fileCtime'};
    $.ajax({
      url: "/api",
      data: dataValue,
      type:"post",
      complete: function(data,str){
        if(data.status == 200){
          if(data.responseJSON != undefined){
            window.tip.show_pk("success",null,window.lan['Cancel query call info success!'] );
            var obj= data.responseJSON;
            $('#'+tid).bootstrapTable('load',obj);
          }else{
            window.tip.show_pk("danger",null,window.lan['Cancel query call info failed!'] +  window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }});
  }
  function delItem(tid,dirName){
    var rows=$('#'+tid).bootstrapTable('getSelections');
    if(rows.length==0) {
      window.tip.show_pk("warning", null, window.lan['You must select a item!']);
      return;
    }else {
      var items = [];
      for (var i = 0; i < rows.length; i++) {
        //修正文件名匹配
        var arr = rows[i].fileName.split('.')[0].split('_');
        arr.pop();
        var fileNameRex = arr.join('_') + '*';
        items.push(rows[i].parentFileName + '/' + fileNameRex);
      }
      rf.createDelItemConfirm(function(){
        $('#myModal').modal().css({
          width: 'auto',
          backdrop:false
        });
        $('#myModal button[name=commit]').bind("click",function(){
          $('#myModal button[name=close]').trigger("click");
          window.tip.show_pk("info",60,window.lan['Deleting items...'],true);
          $.ajax({
            url: "/api",
            traditional : true,
            type: 'post',
            data:{action:'deleteRecordItems',items:items},
            complete: function(data,str){
              if(data.status == 200){
                if(data.responseJSON && data.responseJSON.success){
                  $('#'+tid).bootstrapTable('refresh');
                  window.tip.show_pk("success",null,window.lan['Delete items success!']);
                }else{
                  window.tip.show_pk("danger",null,window.lan['Delete items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
                }
              }else{
                window.tip.show_pk("danger",null,window.lan['Network error!']);
              }
            }});
        });
      })
    }
  }
  function emptyItem(tid,dirName){
    rf.createEmptyItemConfirm(function(){
      $('#myModal').modal().css({
        width: 'auto',
        backdrop:false
      });
      $('#myModal button[name=commit]').bind("click",function(){
        $('#myModal button[name=close]').trigger("click");
        window.tip.show_pk("info",60,window.lan['Emptying items...'],true);
        $.ajax({
          url: "/api",
          traditional : true,
          type: 'post',
          data:{action:'emptyRecordItems',dirName:dirName},
          complete: function(data,str){
            if(data.status == 200){
              if(data.responseJSON && data.responseJSON.success){
                $('#'+tid).bootstrapTable('refresh');   //ˢ���б�
                window.tip.show_pk("success",null,window.lan['Empty items success!']);
              }else{
                window.tip.show_pk("danger",null,window.lan['Empty items failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            }else{
              window.tip.show_pk("danger",null,window.lan['Network error!']);
            }
          }});
      });
    })
  }
  return{
    createCallInfoDetail:createCallInfoDetail
  }
})