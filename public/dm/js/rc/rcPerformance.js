/**
 * Created by Rainc on 2015/9/21.
 */
define(['echarts-mobile'],function(){
  //设置图legend的位置
  function setLegend(target){
    if(document.body.clientWidth < 868){
      target.setOption({
        legend: {
          orient: 'vertical',
          x:'right',
          y:'top'
        }
      })
    }else{
      target.setOption({
        legend: {
          orient: 'horizontal',
          x:'center',
          y:'top'
        }
      })
    }
  }
  function createPerformanceStatistic(pid,id,obj,cb){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        window.performance_statistic_line = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Comprehensive performance statistics(%)'],
            x:'left',
            y:'top',
            textStyle:{
              //fontSize: 14,
//						    fontWeight: 'bolder',
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['cpu userate'],window.lan['server memory userate'],window.lan['disk userate'],
              window.lan['loadavg1'],window.lan['loadavg5'],window.lan['loadavg15']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["generate_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['cpu userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["cpuUtilization"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['server memory userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["memoryUtilization"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['disk userate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["diskUtilization"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['loadavg1'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["loadavg_1m"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['loadavg5'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["loadavg_5m"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['loadavg15'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["loadavg_15m"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        window.performance_statistic_line .setOption(option);
        setLegend(window.performance_statistic_line);
        $(window).resize(function(){
          window.performance_statistic_line .resize();
          setLegend(window.performance_statistic_line);
        });
        cb();
      }
    );
  }
  function createFlowStatistic(pid,id,obj,cb){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        window.flow_statistic_line = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Comprehensive flow statistics'],
            x:'left',
            y:'top',
            textStyle:{
              //fontSize: 14,
//						    fontWeight: 'bolder',
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['rx_rate'],window.lan['tx_rate']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["generate_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['rx_rate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["rx_rate"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['tx_rate'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["tx_rate"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        window.flow_statistic_line .setOption(option);
        setLegend(window.flow_statistic_line);
        $(window).resize(function(){
          window.flow_statistic_line .resize();
          setLegend(window.flow_statistic_line);
        });
        cb();
      }
    );
  }
  function loadLocalData(pid,obj){
    var pn=$("#"+pid);
    if(!pn) return;
    var html='<div class="my-tab"><div class="container-fluid" >'
      +'<div class="row" >'
      +'<div class="col-md-12" style="margin-top: 0px">'
      +'<div class="btn-group" style="margin-bottom: 20px">'
    html += '<button id="go_1_hour_performance" type="button" title="' + window.lan['1 hour'] + '" class="btn btn-info btn-sm tooltip-info"><i class="fa fa-hourglass-start  bigger-100"></i>&nbsp;' + window.lan['1 hour'] + '</button>'
    html += '<button id="go_1_day_performance" type="button" title="' + window.lan['1 day'] + '" class="btn btn-warning btn-sm tooltip-warning"><i class="fa fa-hourglass-half bigger-100"></i>&nbsp;' + window.lan['1 day'] + '</button>'
    html += '<button id="go_1_week_performance" type="button" title="' + window.lan['1 week'] + '" class="btn btn-danger btn-sm tooltip-danger"><i class="fa fa-hourglass-end bigger-100"></i>&nbsp;' + window.lan['1 week'] + '</button>'
    +'</div>'
      +'<div style="width:100%">'
      +'<span id="performance_line" style="display:inline-block;height:250px;width:100%"></span>'
      +'<span id="flow_line" style="display:inline-block;height:250px;width:100%"></span>'
      +'</div>'
      +'</div>'
      +'</div>'
      +'</div></div>';

    pn.html("");
    pn.append(html);

    //生成综合性能统计折线图
    createPerformanceStatistic(pid,'performance_line',obj,function(){
      //生成综合流量统计折线图
      createFlowStatistic(pid,'flow_line',obj,function(){
        //实现多图联动
        window.performance_statistic_line.connect([window.flow_statistic_line]);
        window.flow_statistic_line.connect([window.performance_statistic_line]);
      });
    });

    $('#go_1_hour_performance').bind('click',function(e){
      showPerformanceStatistic(pid,'1 HOUR');
    })
    $('#go_1_day_performance').bind('click',function(e){
      showPerformanceStatistic(pid,'1 DAY');
    })
    $('#go_1_week_performance').bind('click',function(e){
      showPerformanceStatistic(pid,'1 WEEK');
    })
  }
  function loadRemoteData(pid,id){
    showPerformanceStatistic(pid,'1 HOUR');

    //设置定时器，每60秒刷新一次
    window['performance_statistic_timer'] = setInterval(function(){
      //判断performance_statistic是否当前激活的tab，否则清除定时器
      if($("a[href='#record_performance']").parent().attr("class")!= "active"){
        clearInterval(window['performance_statistic_timer'])
        return;
      }
      $.ajax({
        url:'/api',
        data:{action:'getPerformanceCurrentStatistic'},
        type:'POST',
        complete:function(data,str){
          if(data && data.status == 200){
            if(data.responseJSON && data.responseJSON.success){
              var performanceObj = [],flowObj = [],obj=data.responseJSON.result;
              var statusLineData = window.performance_statistic_line.getSeries();
              var keepLineData = false;
              //判断数据量是否超过一个星期的量，若是则不改变数据量，否则增加数据量
              if(statusLineData[0].data.length < 60*24*7) {
                keepLineData = true
              }
              for(var i =0;i< obj.length;i++){
                performanceObj.push([0,obj[i]['cpuUtilization'],false,keepLineData,window.format.UTCStaticToLocal(obj[i]["generate_time"])]);
                performanceObj.push([1,obj[i]['memoryUtilization'],false,keepLineData]);
                performanceObj.push([2,obj[i]['diskUtilization'],false,keepLineData]);
                performanceObj.push([3,obj[i]['loadavg_1m'],false,keepLineData]);
                performanceObj.push([4,obj[i]['loadavg_5m'],false,keepLineData]);
                performanceObj.push([5,obj[i]['loadavg_15m'],false,keepLineData]);
                flowObj.push([0,obj[i]['rx_rate'],false,keepLineData,window.format.UTCStaticToLocal(obj[i]["generate_time"])]);
                flowObj.push([1,obj[i]['tx_rate'],false,keepLineData]);
              }
              window.performance_statistic_line.addData(performanceObj);
              window.flow_statistic_line.addData(flowObj);
            }else{
              window.tip.show_pk("danger",null,window.lan['Failed to get the performance statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
            }
          }else{
            window.tip.show_pk("danger",null,window.lan['Network error!']);
          }
        }
      })
    }, 60 *1000)
  }
  function showPerformanceStatistic(pid,dateRange){
    $.ajax({
      url:'/api',
      data:{action:'getPerformanceStatistic',dateRange:dateRange},
      type:'POST',
      complete:function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            var obj=data.responseJSON.result;
            loadLocalData(pid,obj);
          }else{
            window.tip.show_pk("danger",null,window.lan['Failed to get the performance statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }
    })
  }
  return {
    createPerformanceStatistic:createPerformanceStatistic,
    loadRemoteData:loadRemoteData
  }
})