/**
 * Created by Rainc on 2015/9/21.
 */
define([],function (){
  function createHtml(pid){
    //var pn=$("#main_search");
    //pn.html("");

    createTabHtml(pid);
  }
  function createTabHtml(pid){
    var pn=$("#"+pid);
    pn.html("");
    var html='<ul id="myTab" class="nav nav-tabs">'
      +'<li role="presentation"><a href="#record_config"><i class="fa fa-cog"></i>&nbsp;'+window.lan["recordConifg"]+'</a></li>'
      +'<li role="presentation"><a href="#record_info"><i class="fa fa-table"></i>&nbsp;'+window.lan["recordInfo"]+'</a></li>'
      +'<li role="presentation"><a href="#record_alarm"><span id="checkSerious" class="my-blink"><i class="fa fa-warning"></i></span>&nbsp;'+window.lan["recordAlarm"]+'</a></li>'
      +'<li role="presentation"><a href="#record_performance"><i class="fa fa-line-chart"></i>&nbsp;'+window.lan["recordPerformance"]+'</a></li>'
      +'<li role="presentation"><a href="#record_statistics"><i class="fa fa-line-chart"></i>&nbsp;'+window.lan["recordStatistics"]+'</a></li>'
      +'</ul>'
      +'<div  class="tab-content">'
      +'<div class="tab-pane fade in"  id="record_config" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="record_info" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="record_alarm" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="record_performance" >'
      +'</div>'
      +'<div class="tab-pane fade in"  id="record_statistics" >'
      +'</div>'
      +'</div>';
    pn.append(html);
    //同时是否有严重告警
    checkSeriousExist();
  }
  function checkSeriousExist(){
    //当有严重告警时，闪烁告警标识
    $.ajax({
      url:"/api",
      data:{action:'checkSeriousExist'},
      type:'POST',
      complete:function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            var obj = data.responseJSON.result;
            if(obj.exist){
              $('#checkSerious').addClass('my-blink')
            }else{
              $('#checkSerious').removeClass('my-blink')
            }
          }else{
            //判断严重告警时，先判断数据库是否连接出错，当连接数据库发生错误时，web持续显示数据库连接失败，
            //若未数据库连接问题，显示其他问题
            if(data.responseJSON.errMsg.code == 'ECONNREFUSED'){
              window.tip.show_pk("danger",3600,window.lan['Database connects failed!It is retrying...'],true);
            }else if(data.responseJSON.errMsg == 'Session timeout!Please relogin!'){
              window.location.href = '/';
            }else{
              window.tip.show_pk("danger",3,window.lan['check serious alarm failed!']+ window.format.getErrmsg(data.responseJSON.errMsg));
            }
            setInterval(function(){
              window.location.reload();
            },20 * 1000)
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }
    })
  }
  function tabAfterShow(id){
    if(id=="#record_info"){
      require(["rc-info"], function(ri) {
        ri.createRecordInfo(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#record_config'){
      require(["rc-config"], function(rc) {
        rc.loadRemoteData(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#record_alarm'){
      require(["rc-alarm"], function(ra) {
        ra.createAlarmLogList(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#record_performance'){
      require(["rc-performance"], function(rp) {
        rp.loadRemoteData(id.substring(1),id.substring(1)+"_child");
      });
    }else if(id == '#record_statistics'){
      require(["rc-statistics"], function(rs) {
        rs.loadRemoteData(id.substring(1),id.substring(1)+"_child");
      });
    }
  }
  function procTab(){
    $('#myTab a').bind("shown.bs.tab",function(){
      var id=$(this).attr("href");
      tabAfterShow(id);
    });

    $('#myTab a').click(function(e) {
      e.preventDefault()
      $(this).tab('show');
    });

    setTimeout(function(){
      $('#myTab a:eq(0)').trigger("click");
    },300)
  }
  function init(){
    window.tabAfterShow=tabAfterShow;
    createHtml("my-tab-position");
    procTab();
  }
  return {
    createHtml:createHtml,
    createTabHtml:createTabHtml,
    procTab:procTab,
    init:init,
    tabAfterShow:tabAfterShow,
    checkSeriousExist:checkSeriousExist
  };
});