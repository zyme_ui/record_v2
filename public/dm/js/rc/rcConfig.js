/**
 * Created by Rainc on 2015/9/21.
 */
define(["form-field","base64"],function(field,base64){
  function createConfigurationPanel(pid,id,obj){
    var pn=$("#"+pid);
    if(!pn) return;
    var html='<form id="'+id+'" class="my-tab" role="form">'
      +'<div class="btn-group">'
      +'<button type="button"  name="save" title="' + window.lan['save'] +'" class="btn btn-success btn-sm tooltip-success"><i class="fa fa-save bigger-100"></i>&nbsp;' + window.lan['save'] +'</button>'
      +'<button name="refresh" type="button" title="' + window.lan['refresh'] +'" class="btn btn-warning btn-sm tooltip-warning"><i class="fa fa-refresh bigger-100"></i>&nbsp;' + window.lan['refresh'] +'</button>'
      +'<button name="restartNow" type="button" title="' + window.lan['restart now'] +'" class="btn btn-danger btn-sm tooltip-warning"><i class="fa fa-history bigger-100"></i>&nbsp;' + window.lan['restart now'] +'</button>'
      +'</div>'
      +'<div class="container-fluid" style="margin-top: 20px">'
      +'<div class="row">'

      +'<div class="col-md-6">'
      +'<div name="basic" ><h4 style="font-weight: bold">' + window.lan['Basic Configuration'] + '</h4>'
      +field.getTextField("server_ip","",window.lan['serverIp'],"","",true)
      +field.getTextField("server_port","",window.lan['serverPort'],"","",true)
      +field.getTextField("web_port","",window.lan['web port'],"","",true)
      +field.getTextField("db_host","",window.lan['db host'],"","",true)
      +field.getTextField("db_user","",window.lan['db user'],"","",true)
      +field.getTextField("db_passwd","",window.lan['db password'],"","",true)
      +field.getTextField("db_database","",window.lan['db database'],"","",true)
      +field.getTextField("max_sessions","",window.lan['max_sessions'],"","",false)
      +'</div>'
      +'</div>'
      +'<div class="col-md-6">'
      +'<div name="alarmRate" ><h4 style="font-weight: bold">' + window.lan['Alarm rate Configuration']
      + ' (' + window.lan['No alarm when it is zero!'] + ')' + '</h4>'
      +field.getTextField("cpuAlarmRate","",window.lan['cpu alarm rate(%)'])
      +field.getTextField("serverMemoryAlarmRate","",window.lan['server memory alarm rate(%)'])
      +field.getTextField("diskUsedAlarmRate","",window.lan['disk used alarm rate(%)'])
      + '<br><h4 style="font-weight: bold">' + window.lan['Cloud Configuration'] +
      ' (' + window.lan['No configuration when it is not connects to cloud!'] + ')' + '</h4>'
      +field.getTextField("connect_to_cloud","",window.lan['connect_to_cloud'])
      +field.getTextField("cloudServerHost","",window.lan['cloudServerHost'])
      +field.getTextField("cloudServerPort","",window.lan['cloudServerPort'])
      +field.getTextField("cloudServerPath","",window.lan['cloudServerPath'])
      +field.getTextField("server_id","",window.lan['server_id'])
      +'</div>'
      +'</div>'

      +'</div>'
      +'</div>'
      +'</form>';
    pn.html("");
    pn.append(html);

    $('#'+id).autofill(obj);

    //表单验证
    $('#'+id).validate({
      rules: {
        server_ip:{
          required:true,
          domainNameOrIp:true
        },
        server_port:{
          required:true,
          digits:true,
          range:[1024,65535]
        },
        web_port:{
          required:true,
          digits:true,
          range:[1024,65535]
        },
        db_host:{
          required:true,
          domainNameOrIp:true
        },
        db_user:{
          required:true
        },
        db_passwd:{
          required:true
        },
        db_database:{
          required:true
        },
        cpuAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        },
        serverMemoryAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        },
        diskUsedAlarmRate:{
          required:true,
          digits:true,
          range:[0,100]
        },
        connect_to_cloud:{
          required:true,
          connectToCloud:true
        },
        cloudServerHost:{
          required:false,
          domainNameOrIp:true
        },
        cloudServerPort:{
          required:false,
          digits:true,
          range: [0,65535]
        },
        cloudServerPath:{
          required:false,
          urlPath:true
        },
        server_id:{
          required:false,
          digits:true
        },
        max_sessions:{
          required:true,
          digits:true
        }
      },
      messages: {
        server_ip:{
          required:window.lan['server ip is required!'],
          domainNameOrIp:window.lan['domainName_ip_valid']
        },
        server_port:{
          required:window.lan['server port is required!'],
          digits: window.lan['port_range'],
          range: window.lan['port_range']
        },
        web_port:{
          required:window.lan['web port is required!'],
          digits: window.lan['port_range'],
          range: window.lan['port_range']
        },
        db_host:{
          required:window.lan['db host is required!'],
          domainNameOrIp: window.lan['domainName_ip_valid']
        },
        db_user:{
          required:window.lan['db user is required!']
        },
        db_passwd:{
          required:window.lan['db password is required!']
        },
        db_database:{
          required:window.lan['db database is required!']
        },
        cpuAlarmRate:{
          required:window.lan['CPU alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        },
        serverMemoryAlarmRate:{
          required:window.lan['server memory alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        },
        diskUsedAlarmRate:{
          required:window.lan['disk used alarm rate is required!'],
          digits: window.lan['rate_range'],
          range: window.lan['rate_range']
        },
        connect_to_cloud:{
          required: window.lan['connect_to_cloud_required'],
          connectToCloud:window.lan['connectToCloud_valid']
        },
        cloudServerHost:{
          required:window.lan['cloudServerHost_required'],
          domainNameOrIp:window.lan['domainName_ip_valid']
        },
        cloudServerPort:{
          required: window.lan['cloudServerPort_required'],
          digits: window.lan['digits_valid'],
          range: window.lan['cloudServerPort_range']
        },
        cloudServerPath:{
          required: window.lan['cloudServerPath_required'],
          urlPath: window.lan['urlPath_valid']
        },
        server_id:{
          required: window.lan['server_id_required'],
          digits: window.lan['digits_valid']
        },
        max_sessions:{
          required: window.lan['max_session_required'],
          digits: window.lan['digits_valid']
        }
      }
    });

    $("#"+id+" button[name=save]").bind('click',function(){
      //检查是否通过验证
      if($('#' + id).valid()) {
        var form = $("#" + id);
        //加密数据库密码
        $('#' + id + ' input[name=db_passwd]').val(base64.base64_encode($('#' + id + ' input[name=db_passwd]').val()));
        var pa = form.formSerialize();
        pa += '&action=saveServerConfiguration';
        $.ajax({
          url: "/api",
          type: "POST",
          data: pa,
          complete: function (data, str) {
            if (data.status == 200) {
              if (data.responseJSON && data.responseJSON.success) {
                $("#"+id+" button[name=refresh]").trigger('click');
                window.tip.show_pk("success", null, window.lan['Save server configuration success!']);
              } else {
                window.tip.show_pk("danger", null, window.lan['Save server configuration failed!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            } else {
              window.tip.show_pk("danger", null, window.lan['Network error!']);
            }
          }});
      }else{
        window.tip.show_pk("danger",null,window.lan['Validation failed!']);
      }
    });

    $("#"+id+" button[name=restartNow]").bind('click',function(e){
      var pn=$("#myModal");
      if(!pn) return;
      var html="<div class='modal-dialog'>" +
        "<div class='modal-content'><div class=\"modal-header\">" +
        "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
        "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['Confirm to restart server?'] + "</h4></div>" +
        "<div class=\"\"></form></div>" +
        "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
        "<button name=\"commit\" type=\"button\" class=\"btn btn-danger\">" + window.lan['confirm'] + "</button></div></div><!-- /.modal-content --></div>"
      pn.html("");
      pn.append(html);

      $('#myModal').modal().css({
        width: 'auto',
        backdrop:false
      });

      $('#myModal button[name=commit]').bind("click",function(){
        $('#myModal button[name=close]').trigger("click");
        window.tip.show_pk("info", 60, window.lan['server restarting...please refresh later!'],true);
        $.ajax({
          url: "/api",
          type: "POST",
          data: {action:'restartNow'},
          complete: function (data, str) {
            if (data.status == 200) {
              if (data.responseJSON && data.responseJSON.success) {
                window.tip.show_pk("success", null, window.lan['server restart success!Please refresh page!']);
                //服务重启后刷新页面
                setTimeout(function(){
                  window.location.reload();
                },3000)
              } else {
                if(data.responseJSON.errMsg == 'Session timeout!Please relogin!'){
                  window.location.reload();
                }else{
                  window.tip.show_pk("danger", null, window.lan['server restart failed!'] +
                  window.format.getErrmsg(data.responseJSON.errMsg));
                }
              }
            } else {
              //服务重启后刷新页面
              setTimeout(function(){
                window.location.reload();
              },3000)
              //window.tip.show_pk("danger", null, window.lan['Network error!']);
            }
          }});
      })
    })

  }
  function loadLocalData(pid,id,obj){
    createConfigurationPanel(pid,id,obj);
  }
  function loadRemoteData(pid,id){
    $.ajax({
      url:"/api",
      data:{action:'getServerConfiguration'},
      type:'post',
      complete:function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            //将数据库密码解密后显示
            data.responseJSON.result.db_passwd = base64.base64_decode(data.responseJSON.result.db_passwd);
            loadLocalData(pid,id,data.responseJSON.result);
            $("#"+id+" button[name=refresh]").bind('click',function(){
              loadRemoteData(pid,id);
            });
          }else{
            var errMsg = data.responseJSON.errMsg;
            if(errMsg == 'Session timeout!Please relogin!'){
              window.location.href = '/'
            }else{
              window.tip.show_pk("danger",null,window.lan['Get server configuration failed!'] + window.format.getErrmsg(data.responseJSON.errMsg))
            }
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }})
  }
  return{
    loadRemoteData:loadRemoteData
  }
})