/**
 * Created by Rainc on 2015/9/21.
 */
define(["form-field"],function(field){
  function createDelItemConfirm(cb){
    var pn=$("#myModal");
    if(!pn) return;
    var html="<div class='modal-dialog'>" +
      "<div class='modal-content'><div class=\"modal-header\">" +
      "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
      "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['Confirm to delete items?'] + "</h4></div>" +
      "<div class=\"\"></form></div>" +
      "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
      "<button name=\"commit\" type=\"button\" class=\"btn btn-danger\">" + window.lan['confirm'] + "</button></div></div><!-- /.modal-content --></div>"
    pn.html("");
    pn.append(html);
    cb();
  }
  function createEmptyItemConfirm(cb){
    var pn=$("#myModal");
    if(!pn) return;
    var html="<div class='modal-dialog'>" +
      "<div class='modal-content'><div class=\"modal-header\">" +
      "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
      "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['Confirm to empty items?'] + "</h4></div>" +
      "<div class=\"\"></form></div>" +
      "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
      "<button name=\"commit\" type=\"button\" class=\"btn btn-danger\">" + window.lan['confirm'] + "</button></div></div><!-- /.modal-content --></div>"
    pn.html("");
    pn.append(html);
    cb();
  }
  function createExportItems(cb){
    var pn=$("#myModal");
    if(!pn) return;
    var html='<div class="modal-dialog">'
      +'<div class="modal-content">'
      +'<div class="modal-header">'
      +'<button type="button" class="close" '
      +'data-dismiss="modal" aria-hidden="true">'
      +'&times;'
      +'</button>'
      +'<h4 class="modal-title" id="myModalLabel">'
      +window.lan['export call ticket']
      +'</h4>'
      +'</div>'
      +'<div class="modal-body">'
      +'<div class="" id="" role="form">';
    html+= field.getRadioField("exportType","csv",window.lan['export format'],[{value:"csv",text:"csv"},{value:"txt",text:"txt"}]);
    html+= '<a download="" href=""><button id="exportButton" style="display: none"></button></a>'
    html+='</div>'
    +'</div>'
    +'<div class="modal-footer">'
    +'<button name="close" type="button" class="btn btn-default" '
    +'data-dismiss="modal">' + window.lan['close']
    +'</button>'
    +'<button name="commit" type="button" class="btn btn-primary">'
    +window.lan['submit']
    +'</button>'
    +'</div>'
    +'</div><!-- /.modal-content -->'
    +'</div>';
    pn.html("");
    pn.append(html);
    cb();
  }
  function createConfirmAlarmConfirm(cb){
    var pn=$("#myModal");
    if(!pn) return;
    var html="<div class='modal-dialog'>" +
      "<div class='modal-content'><div class=\"modal-header\">" +
      "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
      "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['Confirm to confirm alarms?'] + "</h4></div>" +
      "<div class=\"\"></form></div>" +
      "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
      "<button name=\"commit\" type=\"button\" class=\"btn btn-danger\">" + window.lan['confirm'] + "</button></div></div><!-- /.modal-content --></div>"
    pn.html("");
    pn.append(html);
    cb();
  }
  function createDelAlarmConfirm(cb){
    var pn=$("#myModal");
    if(!pn) return;
    var html="<div class='modal-dialog'>" +
      "<div class='modal-content'><div class=\"modal-header\">" +
      "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
      "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['Confirm to delete alarms?'] + "</h4></div>" +
      "<div class=\"\"></form></div>" +
      "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
      "<button name=\"commit\" type=\"button\" class=\"btn btn-danger\">" + window.lan['confirm'] + "</button></div></div><!-- /.modal-content --></div>"
    pn.html("");
    pn.append(html);
    cb();
  }
  function createEmptyAlarmConfirm(cb){
    var pn=$("#myModal");
    if(!pn) return;
    var html="<div class='modal-dialog'>" +
      "<div class='modal-content'><div class=\"modal-header\">" +
      "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">×</button>" +
      "<h4 class=\"modal-title\" id=\"myModalLabel\">" + window.lan['Confirm to empty alarms?'] + "</h4></div>" +
      "<div class=\"\"></form></div>" +
      "<div class=\"modal-footer\"><button name=\"close\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + window.lan['cancel'] + "</button>" +
      "<button name=\"commit\" type=\"button\" class=\"btn btn-danger\">" + window.lan['confirm'] + "</button></div></div><!-- /.modal-content --></div>"
    pn.html("");
    pn.append(html);
    cb();
  }
  return {
    createDelItemConfirm:createDelItemConfirm,
    createEmptyItemConfirm:createEmptyItemConfirm,
    createConfirmAlarmConfirm:createConfirmAlarmConfirm,
    createDelAlarmConfirm:createDelAlarmConfirm,
    createEmptyAlarmConfirm:createEmptyAlarmConfirm
  }
})