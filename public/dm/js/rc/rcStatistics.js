/**
 * Created by Rainc on 2015/9/21.
 */

define(['echarts-mobile'],function(){
  function createStatusStatistic(pid,id,obj,cb){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        window.service_status_statistic_line = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Comprehensive deal status statistics'],
            x:'left',
            y:'top',
            textStyle:{
              //fontSize: 14,
//						    fontWeight: 'bolder',
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['sessions_count'],window.lan['his_sessions_count'],window.lan['his_success_count'],
              window.lan['his_failed_count']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["generate_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['sessions_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["sessions_count"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['his_sessions_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["his_sessions_count"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['his_success_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["his_success_count"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['his_failed_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["his_failed_count"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        window.service_status_statistic_line.setOption(option);
        $(window).resize(function(){
          window.service_status_statistic_line.resize();
        });
        cb();
      }
    );
  }
  function loadLocalData(pid,obj){
    var pn=$("#"+pid);
    if(!pn) return;
    var html='<div class="my-tab"><div class="container-fluid" >'
      +'<div class="row" >'
      +'<div class="col-md-12" style="margin-top: 0px">'
      +'<div class="btn-group" style="margin-bottom: 20px">'
    html += '<button id="go_dev_list" type="button" title="' + window.lan['According to device'] + '" class="btn btn-info btn-sm tooltip-info"><i class="fa fa-cubes bigger-100"></i>&nbsp;' + window.lan['According to device'] + '</button>'
    html += '<button id="go_1_hour_service" type="button" title="' + window.lan['1 hour'] + '" class="btn btn-info btn-sm tooltip-info"><i class="fa fa-hourglass-start  bigger-100"></i>&nbsp;' + window.lan['1 hour'] + '</button>'
    html += '<button id="go_1_day_service" type="button" title="' + window.lan['1 day'] + '" class="btn btn-warning btn-sm tooltip-warning"><i class="fa fa-hourglass-half bigger-100"></i>&nbsp;' + window.lan['1 day'] + '</button>'
    html += '<button id="go_1_week_service" type="button" title="' + window.lan['1 week'] + '" class="btn btn-danger btn-sm tooltip-danger"><i class="fa fa-hourglass-end bigger-100"></i>&nbsp;' + window.lan['1 week'] + '</button>'
    +'</div>'
    +'<div style="width:100%">'
    +'<span id="service_status_line" style="display:inline-block;height:250px;width:100%"></span>'
    +'</div>'
    +'</div>'
    +'</div>'
    +'</div></div>';

    pn.html("");
    pn.append(html);

    //生成综合服务统计折线图
    createStatusStatistic(pid,'service_status_line',obj,function(){
    });

    //跳转到设备列表视图
    $('#go_dev_list').bind('click',function(e){
      require(['rc-deviceList'],function(rd){
        rd.createDeviceList(pid,pid+'_child');
      })
    })
    $('#go_1_hour_service').bind('click',function(e){
      showServiceStatistic(pid,'1 HOUR');
    })
    $('#go_1_day_service').bind('click',function(e){
      showServiceStatistic(pid,'1 DAY');
    })
    $('#go_1_week_service').bind('click',function(e){
      showServiceStatistic(pid,'1 WEEK');
    })
  }
  function loadRemoteData(pid,id){
    showServiceStatistic(pid,'1 HOUR');

    //设置定时器，每5秒刷新一次
    if(!window['service_statistic_timer']){
      window['service_statistic_timer'] = setInterval(function(){
        //判断service_statistic是否当前激活的tab，否则清除定时器
        if($("a[href='#record_statistics']").parent().attr("class")!= "active"){
          clearInterval(window['service_statistic_timer'])
          return;
        }
        $.ajax({
          url:'/api',
          data:{action:'getServiceCurrentStatistic'},
          type:'POST',
          complete:function(data,str){
            if(data && data.status == 200){
              if(data.responseJSON && data.responseJSON.success){
                var statusObj = [], obj=data.responseJSON.result;
                var statusLineData = window.service_status_statistic_line.getSeries();
                var keepLineData = false;
                //判断数据量是否超过一个星期的量，若是则不改变数据量，否则增加数据量
                if(statusLineData[0].data.length < 60*24*7) {
                  keepLineData = true
                }

                for(var i =0;i< obj.length;i++){
                  statusObj.push([0,obj[i]['sessions_count'],false,keepLineData,window.format.UTCStaticToLocal(obj[i]["generate_time"])]);
                  statusObj.push([1,obj[i]['his_sessions_count'],false,keepLineData]);
                  statusObj.push([2,obj[i]['his_success_count'],false,keepLineData]);
                  statusObj.push([3,obj[i]['his_failed_count'],false,keepLineData]);
                }

                window.service_status_statistic_line.addData(statusObj);
              }else{
                window.tip.show_pk("danger",null,window.lan['Failed to get the service statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
              }
            }else{
              window.tip.show_pk("danger",null,window.lan['Network error!']);
            }
          }
        })
      }, 60 * 1000)
    }
  }
  function showServiceStatistic(pid,dateRange){
    $.ajax({
      url:'/api',
      data:{action:'getServiceStatistic',dateRange:dateRange},
      type:'POST',
      complete:function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            var obj=data.responseJSON.result;
            loadLocalData(pid,obj);
          }else{
            window.tip.show_pk("danger",null,window.lan['Failed to get the service statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }
    })
  }
  return {
    loadRemoteData:loadRemoteData
  }
})