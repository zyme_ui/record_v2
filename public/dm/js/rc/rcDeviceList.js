/**
 * Created by Rainc on 2015/9/24.
 */
/**
 * Created by Rainc on 2015/7/27.
 */
define(["bootstrap-table"],function(){
  function createDeviceList(pid,id){
    var pn=$("#"+pid);
    if(!pn){
      return;
    }
    var html= '<h2>' + window.lan['IP list'] + '</h2>';
    html += '<div id="'+id+'-toolbar" class="btn-group" role="group" aria-label="...">'
    + '<button id="'+id+'-backToService" class="btn btn-success btn-sm" title="' + window.lan['go back'] + '"><i class="fa fa-chevron-circle-left bigger-100"></i>&nbsp;' + window.lan['go back'] + '</button>'
    + '</div>';
    html+='<table id='+id+'></table>';
    pn.html("");
    pn.append(html);

    $('#'+id).bootstrapTable({
      method: 'post',
      contentType:'application/x-www-form-urlencoded;charset=UTF-8',
      url: '/api',
      cache: false,
      responseHandler:function(res){
        if(res.success) {
          if (res['result']["total"] != 0 && res.result.list.length == 0) {
            $('#' + id).bootstrapTable('selectPage', 1);
            return {rows: [], total: 0}
          } else {
            var obj={};
            if(res && res.result.list && res.result.list.length>0){
              obj["rows"]=res.result.list;
              obj.total=res['result']["total"];
              return obj;
            }else{
              obj["rows"]= [];
              obj.total=res['result']["total"];
              return obj;
            }
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['get call bill statistic failed!'] + window.format.getErrmsg(res.errMsg));
          if(res.errMsg == 'Session timeout!Please relogin!'){
            setTimeout(function(){
              window.location.reload();
            },2000)
          }
        }
      },
      queryParams:function(p){
        p.action = 'getDeviceList';
        return p;
      },
      //height:515,
      striped: true,
      toolbar:"#"+id+"-toolbar",
      pagination: true,
      pageSize: 10,
      pageList: [10, 50,200,500,1000,'All'],
      search: true,
      showColumns: true,
      showRefresh: true,
      showToggle:true,
      queryParamsType:'limit',
      sidePagination: "server",
      smartDisplay:true,
      minimumCountColumns: 2,
      undefinedText: '-',
      clickToSelect: false,
      detailView:true,
      detailFormatter:function(index,row){
        var html = getHtml(index,row);
        return html;
      },
      columns: [{
        field: 'uuid',
        title: 'id',
        align: 'center',
        valign: 'middle',
        sortable: true,
        visible: false
      },{
        field: 'dev_ip',
        title: window.lan['device ip'],
        align: 'center',
        valign: 'middle',
        sortable: true
      },{
        field: 'generate_time',
        title: window.lan['create time'],
        align: 'center',
        valign: 'middle',
        sortable: true,
        formatter:function(value,row,index){
          if(value){
            return window.format.UTCStaticToLocal(value);
          }
          return "---";
        }
      }]
    });
    window.list.changeForAce(pid);
    $(window).resize(function () {
      window.list.changeView(pid,id,600);
    });
    window.list.changeView(pid,id,600);

    //绑定返回事件
    $('#' + id + '-backToService').bind('click',function(e){
      require(['rc-statistics'],function(rs){
        rs.loadRemoteData('record_statistics','record_statistic_child');
      })
    })

    //监听展开事件
    $('#'+id).on('expand-row.bs.table', function (e, index, row, detail) {
      showDeviceStatistic(index,row);
      //设置定时器，每1分钟刷新一次
      window['device_statistic_' + index] = setInterval(function(){
        //定时判断该节点是否还存在，不存在清除定时器
        if( $('#device_status_line_' + index).length == 0){
          clearInterval(window['device_statistic_' + index])
        }
        showDeviceStatistic(index,row);
      },60 * 1000)
    })
  }
  function showDeviceStatistic(index,row){
    var dsid= 'device_status_line_' + index;
    $("#"+dsid).html("");

    var deviceIP = row['dev_ip'];
    //查询单个设备的信息
    $.ajax({
      url: "/api",
      data: {deviceIP:deviceIP,action:"getDeviceStatistic"},
      type:"post",
      complete: function(data,str){
        if(data && data.status == 200){
          if(data.responseJSON && data.responseJSON.success){
            var obj =data.responseJSON.result;
            //生成图表
            createDeviceStatusStatistic(dsid,obj);
          }else{
            window.tip.show_pk("danger",null,window.lan['Failed to get the device statistic!'] + window.format.getErrmsg(data.responseJSON.errMsg));
          }
        }else{
          window.tip.show_pk("danger",null,window.lan['Network error!']);
        }
      }})
  }
  function getHtml(index,row){
    var dsid = "device_status_line_" + index;
    var html= '<div style="width:100%">'
      +'<span id="' + dsid + '" style="display:inline-block;height:250px;width:100%"></span>'
      +'</div>';

    return html;
  }
  function createDeviceStatusStatistic(id,obj){
    require.config({
      paths: {
        "echarts": "echarts-m-1.0.0/source"
      }
    });
    require(
      [
        'echarts',
        'echarts/chart/line'
      ],
      function (ec) {
        var myChart = ec.init(document.getElementById(id));
        option = {
          tooltip : {
            trigger: 'axis'
          },
          title: {
            text: window.lan['Device history statistics'],
            x:'left',
            y:'top',
            textStyle:{
              color: '#707070'
            }
          },
          legend: {
            textStyle:{fontSize:10},
            data:[window.lan['sessions_count'],window.lan['his_sessions_count'],window.lan['his_success_count'],
              window.lan['his_failed_count']]
          },
          dataZoom : {
            show : true,
            realtime : true,
            start : 0,
            end : 100
          },
          xAxis : [
            {
              type : 'category',
              boundaryGap : false,
              data : function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(window.format.UTCStaticToLocal(obj[i]["generate_time"]));
                  }
                }
                return list;
              }()
            }
          ],
          yAxis : [
            {
              type : 'value'
            }
          ],
          series : [
            {
              name: window.lan['sessions_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["sessions_count"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['his_sessions_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["his_sessions_count"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['his_success_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["his_success_count"]);
                  }
                }
                return list;
              }()
            },
            {
              name: window.lan['his_failed_count'],
              type:'line',
              data:function (){
                var list = [];
                if(obj && obj.length){
                  for (var i = 0; i<=obj.length-1; i++) {
                    list.push(obj[i]["his_failed_count"]);
                  }
                }
                return list;
              }()
            }
          ]
        };
        myChart.setOption(option);
        $(window).resize(function(){
          myChart.resize();
        });
      }
    );
  }
  return{
    createDeviceList: createDeviceList
  }
})