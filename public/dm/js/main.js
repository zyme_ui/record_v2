var d=new Date();
var vd=new Date(d.getFullYear(),d.getMonth(),d.getDate(),0,0,0);
require.config({
  baseUrl: "dm",
  urlArgs: "ver=" +  vd.getTime()+"&my=2.7",
  paths: {
    "jquery": "lib/jquery-1.11.2.min",
    "jquery-ui":"lib/jquery-ui.min",
    "bootstrap": "bootstrap-3.3.2-dist/dist/js/bootstrap.min",
    "bootstrap-table": "bootstrap-table/bootstrap-table.min",
    "echarts-mobile": "echarts-m-1.0.0/source/echarts",
    "flat-ui-video":"flat-ui/dist/js/vendor/video",
    "flat-ui-min":"flat-ui/dist/js/flat-ui.min",
    "flat-ui-app":"flat-ui/docs/assets/js/application",
    "jquery-form":"jquery-form/jquery.form",
    "format":"js/format",
    "auto-fill":"auto-fill/jquery.formautofill",
    "datetimepicker":"lib/datetimepicker/jquery.datetimepicker",
    "moment":"moment/moment",
    "bootstrap-tags":"bootstrap-tags/dist/bootstrap-tagsinput.min",
    "bootstrap-tags-app":"bootstrap-tags/examples/assets/app",
    "bootstrap-tags-angular":"bootstrap-tags/dist/bootstrap-tagsinput-angular.min",
    "headroom":"lib/Headroom",
    "sco-message":"lib/scojs/js/sco.message",
    "validate":"js/validate",
    "base64":"js/base64",
    "form-field":"js/FormField",

    "lan-cn":"js/lan/LanCn",
    "lan-en":"js/lan/LanEn",
    "lan-con":"js/lan/LanCon",
    "map":"js/map",
    "tip":"js/tip",
    "progress":"js/progress",
    "form-field":"js/FormField",
    "jquery-tree":"lib/jquery-tree/js/navigation",
    "jquery-validate":"lib/jquery.validate.min",
    "list":"js/list",
    "tree":"js/tree",
    "ajax-file-upload":"lib/ajaxfileupload",
    "tPlayer":"js/tPlayer",

    "user-fun":"js/user/userFun",

    "rc-main":"js/rc/rcMain",
    "rc-alarm":"js/rc/rcAlarm",
    "rc-config":"js/rc/rcConfig",
    "rc-fun":"js/rc/rcFun",
    "rc-info":"js/rc/rcInfo",
    "rc-infoDetail":"js/rc/rcInfoDetail",
    "rc-performance":"js/rc/rcPerformance",
    "rc-statistics":"js/rc/rcStatistics",
    "rc-deviceList":"js/rc/rcDeviceList"
  },
  shim: {
    'jquery-ui':{
      deps: ['jquery']
    },
    'bootstrap': {
      deps: ['jquery'],
      exports: 'bs'
    },
    'bootstrap-table': {
      deps: ['bootstrap'],
      exports: 'bstb'
    },
    'bootstrap-tags': {
      deps: ['bootstrap'],
      exports: 'bt'
    },
    'bootstrap-tags-app': {
      deps: ['bootstrap-tags'],
      exports: 'bta'
    },
    'flat-ui-video': {
      deps: ['jquery'],
      exports: 'fuv'
    },
    'flat-ui-min': {
      deps: ['flat-ui-video'],
      exports: 'fum'
    },
    'flat-ui-app': {
      deps: ['flat-ui-min'],
      exports: 'fua'
    },
    'jquery-validate':{
      deps: ['jquery'],
      exports: 'jv'
    },
    'jquery-form': {
      deps: ['jquery'],
      exports: 'jf'
    },
    'auto-fill': {
      deps: ['bootstrap'],
      exports: 'af'
    },
    'datetimepicker': {
      deps: ['jquery'],
      exports: 'dtp'
    },
    'validate':{
      deps: ['jquery-validate'],
      exports: 'va'
    },
    'tPlayer':{
      deps: ['jquery']
    }
  }
});

Array.prototype.remove = function(s) {
  for (var i = 0; i < this.length; i++) {
    if (typeof(s) == 'object'){
      if (s == this[i]){
        this.splice(i, 1);
        break;
      }
    }else if(typeof(s) == 'number'){
      if (s == i){
        this.splice(i, 1);
        break;
      }
    }
  }
}
require(['jquery'],function(){
  require(['lan-con','format','tip','list','form-field','bootstrap-tags-app','ajax-file-upload','jquery-form','auto-fill','datetimepicker','map',
    'sco-message','validate','jquery-ui'],function(lc,format,tip,list,field){
    lc.initLan(callAfterLan);
    window.lc=lc;
    window.tip=tip;
    window.list=list;
    window.format=format;
    window.field = field;
  })
})

function callAfterLan(){
  $("#logout").html('<i class="fa fa-power-off"></i>&nbsp;'+window.lan["logout"]+'</a>');
  $("#logo").html('<i class="fa fa-file-text"></i>&nbsp;'+window.lan["logo"]+'</a>');
  $("#change_pwd").html('<i class="fa fa-exchange"></i>&nbsp;'+window.lan["changePwd"]+'</a>');
  $("#rc_manage").html('<i class="fa fa-youtube-play"></i>&nbsp;'+window.lan["rcManage"]+'</a>');
  $("#serverVersion").html(window.lan['VERSION:'] + '&nbsp;v1.0 RC2'+'</a>');

  var h=''
//    	+'<img class="nav-user-photo" src="dm/picture/user.jpg" alt="">'
    +'<span class="user-info">'
    +'<small >' + window.lan["welcome"] + '</small>'
//    	+'</br>'
    + 'Admin'
    +'</span>'
    +'<i class="caret"></i>';
  $("#cur_user").html(h);
  $("#cur_user li").bind("click",function(){
    $("cur_user").removeClass("active");
  });
//  $("#lancon li").bind("click",function(){
//    $("#lancon").removeClass("active");
//  });
  $("#logout").bind("click",function(){
    $.ajax({
      url: "/api",
      data:{action:'logoutAction'},
      type:'POST',
      complete:function(data,str){
        if(data.responseJSON.success){
          window.location.href="/";
        }else{
          window.tip.show_pk("danger",null,window.lan['login out error!'] + data.responseJSON.errMsg);
        }
      }
    })
  });
  $("#change_pwd").bind("click",function(){
    var bt=$("button[class=navbar-toggle]");
    if(bt.attr("aria-expanded")=="true"){
      $("button[class=navbar-toggle]").trigger("click");
    }
    require(['user-fun'],function(uf){
      var userId = 1;
      uf.createPanel(userId);
    });
  });

  //headroom导航栏动作
  require(['headroom'],function(hr){
    // 位置
    var myElement = document.querySelector("nav");
    // 新建对象
    var headroom  = new Headroom(myElement);
    // 初始化
    headroom.init();
  })

  $('#bs-example-navbar-collapse a').click(function (e) {
    e.preventDefault();
    var c=$(this);
    var p=$(this).parent().parent();
    var children=p.children();
    var aid=$(this).attr("id");
    children.each(function(){
      $(this).removeClass("active");
    });
    $(this).parent().addClass("active");
    if(aid){
      if(aid == "rc_manage" ){

        var bt=$("button[class=navbar-toggle]");
        if(bt.attr("aria-expanded")=="true"){
          $("button[class=navbar-toggle]").trigger("click");
        }
        //激活模块
        activeModule($(this).html());
        return;
      }
    }

  })

  //首次激活模块
  $("#rc_manage").trigger("click");
}

function activeModule(module){
  $("#my-tab-position").parent().css("margin-top","0px");
  if(module.indexOf(window.lan["rcManage"])>=0){
    require(['rc-main'], function (cm){
      window.cbMain=cm;
      cm.init();
    });
  }
}
