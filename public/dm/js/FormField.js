define(function (){
	function getTextField(name,value,label,placeholder,type,isNecessary,istime,helpText){
		var html='';
		var l=label?label:"";
		var v=value?value:"";
		var n=name?name:"";
		var p=placeholder?placeholder:"";
		var h=helpText?helpText:"";
		var t="text";
		if(type){
			t=type;
		}
		var it="";
		if(istime){
			it="true";
		}
		html+='<div class="form-group-sm">'
		  +'<label class="margin-bottom-0">'
      if(isNecessary){
        html+= '<span style="color:red">*</span>'
      }
      html+= l+'</label>'
		  +'<input istime="'+it+'" type="' + t +  '" class="form-control" '
		  +' name="'+n
		  +'" value="'+v
		  +'" placeholder="'+p+'">'
			+'<p class="help-block">'+h+'</p>'
	  +'</div>';
		return html;
	}
	function getTextareaField(name,value,label,placeholder,isNecessary){
		var html='';
		var l=label?label:"";
		var v=value?value:"";
		var n=name?name:"";
		var p=placeholder?placeholder:"";
		html+='<div class="form-group-sm">'
	      +'<label class="margin-bottom-0">'
      if(isNecessary){
        html += '<span style="color: red">*</span>'
      }
      html+= l+'</label>'
	      +'<textarea name="'+n+'" value="'+v+'" placeholder="'+p+'" class="form-control" style="height:80px;">'
	      +'</textarea>'
	      +'</div>';
		return html;
		
	}
	function getDisplayField(name,value,label,placeholder){
		var html='';
		var l=label?label:"";
		var v=value?value:"";
		var n=name?name:"";
		var p=placeholder?placeholder:"";
		html+='<div class="form-group-sm">'
	      +'<label class="margin-bottom-0">'+l+'</label>'
//	      +'<input type="text" class="form-control display-text"  name="upgradeType" value="'+window.lc.getValue("upgradeType",obj.upgradeType)+'" placeholder="">'
	      +'<p class="form-control display-text"  name="'+n+'"  placeholder="'+p+'">'+v+'</p>'
	      +'</div>';
		
		return html;
	}
	function getCheckboxField(name,value,label,list){
		if(!list){
			return;
		}
		var html='';
		var l=label?label:"";
		var v=value?value:"";
		var n=name?name:"";
		html+='<div class="form-group-sm">'
		+'<label class="margin-bottom-0">'+l+'</label>'
		+'<div >';				  		
		for(var i=0;i<list.length;i++){
			html+='<label class="checkbox-inline margin-bottom-0">';
			var  checked="";
			var disabled="";
			if(value==list[i].value){
				checked="checked";
			}
			if(list[i].disabled){
				disabled="disabled";
			}
			
			html+='<input class="ace ace-checkbox-2" type="checkbox" name="'+n+'" value="'+list[i].value+'" data-toggle="checkbox" '+checked+ ' '+ disabled + '>'
			+'<span class="lbl">&nbsp;'+list[i].text+'</span>';
			html+='</label>';
		}
		html+='</div>'
		+'</div>';
//		console.log(html)
		return html;		
	}
	function getSwitch(name,value,label,inputValue){
		var html='';
		html+='<div class="form-group-sm">'
		      +'<label>'+label+'</label>'
				+'<input name="'+name+'" value="'+inputValue+'" class="ace ace-switch ace-switch-6" type="checkbox">'
				+'<span class="lbl"></span>'
		      +'</div>';
		return html;		
	}
	function getRadioField(name,value,label,list,isNecessary){
		if(!list){
			return;
		}
		var html='';
		var l=label?label:"";
		var v=value?value:"";
		var n=name?name:"";
		html+='<div class="form-group-sm">'
		+'<label class="margin-bottom-0">'
      if(isNecessary){
        html += '<span style="color: red">*</span>'
      }
      html+= l+'</label>'
		+'<div>';				  		
		for(var i=0;i<list.length;i++){
			html+='<label class="radio-inline">';
			if(value==list[i].value){
				html+='<input style="width:18px;height:18px;" type="radio" name="'+n+'" value="'+list[i].value+'" checked data-toggle="radio" >'
				+'<span class="lbl">&nbsp;'+list[i].text+'</span>';
			}else{
				html+='<input style="width:18px;height:18px;" type="radio" name="'+n+'" value="'+list[i].value+'" data-toggle="radio" >'
				+'<span class="lbl">&nbsp;'+list[i].text+'</span>';
			}
			html+='</label>';
		}
		html+='</div>'
		+'</div>';
		
		return html;
	}
	function getComboField(name,value,label,list){
		if(!list){
			return;
		}
		var html='';
		var l=label?label:"";
		var v=value?value:"";
		var n=name?name:"";
		html+='<div class="form-group-sm">'
	      +'<label class="control-label">'+l+'</label>';
		  html +='<select  name="'+n+'" value="'+v+'" class="form-control">';		     
		  //html +='<option value="-1" selected></option>';
		  for(var i=0;i<list.length;i++){
			  var sel="";
			  if(value==list[i].value){
				  sel="selected";
			  }
			  html+='<option '+sel+' value="'+list[i]["value"]+'">'+list[i]["text"]+'</option>';
		  }
	      html+='</select>'
	      +'</div>';
//	      console.log(html);
		return html;
	}
  function insertAtCursor(myField, myValue) {
//IE support
    if (document.selection) {
      myField.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      sel.select();
    }
//MOZILLA/NETSCAPE support
    else if (myField.selectionStart || myField.selectionStart == '0') {
      var startPos = myField.selectionStart;
      var endPos = myField.selectionEnd;
// save scrollTop before insert www.keleyi.com
      var restoreTop = myField.scrollTop;
      myField.value = myField.value.substring(0, startPos) + myValue + myField.value.substring(endPos, myField.value.length);
      if (restoreTop > 0) {
        myField.scrollTop = restoreTop;
      }
      myField.focus();
      myField.selectionStart = startPos + myValue.length;
      myField.selectionEnd = startPos + myValue.length;
    } else {
      myField.value += myValue;
      myField.focus();
    }
  }
	function getSellectText(labelSelectName,labelSelectValue,labelList,selectName,selectValue,selectList,textName,textValue,textPlaceholder,classList){
		var html = '';
		var classValue = "";
		for(var i=0;i<classList.length;i++){
			classValue += classList[i] + ' '
		}
		classValue += ' form-group-sm';
		html += '<div class="' + classValue + '" style="margin-bottom:10px;margin-top:13px"><div style="padding-right: 0px;width:100%">' +
		'<select style="width:35%;border-radius:4px;border:1px solid #ccc;margin-right:10px" name="' + labelSelectName + '" value="' + labelSelectValue + '">';
		for(var i =0;i<labelList.length;i++){
			if(labelList[i].value == labelSelectValue){
				html += '<option selected value="' + labelList[i].value + '"> ' + labelList[i].text + ' </option>'
			}else{
				html += '<option value="' + labelList[i].value + '"> ' + labelList[i].text + ' </option>'
			}
		}
		html += '</select>' +
		'<select style="width:20%;border-radius:4px;border:1px solid #ccc;margin-right:10px" name="' + selectName + '" value="' + selectValue + '">';
		for(var i =0;i<selectList.length;i++){
			if(selectList[i].value == selectValue){
				html += '<option selected value="' + selectList[i].value + '"> ' + selectList[i].text + ' </option>'
			}else{
				html += '<option value="' + selectList[i].value + '"> ' + selectList[i].text + ' </option>'
			}
		}
		html += '</select>' +
		'<input type="text" style="width:35%;border:1px solid #ccc;border-radius:4px;"  name="' + textName + '" value="' + textValue
		+ '" placeholder="' + textPlaceholder + '" />' +
		'</div></div>'
		return html;
	}
  return {
		getTextField:getTextField,
		getTextareaField:getTextareaField,
		getDisplayField:getDisplayField,
		getRadioField:getRadioField,
		getCheckboxField:getCheckboxField,
		getComboField:getComboField,
		getSwitch:getSwitch,
    insertAtCursor:insertAtCursor,
		getSellectText:getSellectText
	};
});


