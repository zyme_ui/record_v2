function getValue(curLan,name){
	var lan;
	if(curLan == 'cn' || curLan == 'CN'){
		lan=lan_cn();
	}else{
		lan=lan_en();
	}
	if(!lan) return;
	var str=name;
	if(lan[str]){
		return lan[str];
	}else if(curLan == 'cn'|| curLan == 'CN'){
		return "未知";
	}else{
		return "Unknow";
	}
}

function getCookie(name)//取cookies函数
{
	var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
	if(arr != null) return unescape(arr[2]); return null;

}

function setCookie(name,value, time)//两个参数，一个是cookie的名子，一个是值
{
	var exp = new Date(); //new Date("December 31, 9998");
	var Days;//cookie 保留的天数
	var month;
	var year;
	if(time == 0)
	{
		Days = 0;
	}
	else if (time=="one day")
	{
		Days = 1;
	}
	else if (time=="one month")
	{
		month = exp.getMonth()+1;
		year =  exp.getFullYear();
		Days = GetDayNum(month,year);
	}
	else if(time=="one year")
	{
		year =  exp.getFullYear();
		if(IsLeapYear(year))
		{
			Days = 366;
		}
		else
		{
			Days = 365;
		}
	}

	exp.setTime(exp.getTime() + Days*24*60*60*1000);
	document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();
}
function GetDayNum(month,year)
{
	if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
	{
		return 31;
	}
	else if(month==4 || month==6 || month==9 || month==11)
	{
		return 30;
	}
	else if(month == 2)
	{
		if(IsLeapYear(year))
		{
			return 29;
		}
		else
		{
			return 28;
		}
	}
	else
	{
		return 0;
	}
}

function IsLeapYear(iYear)
{
	if (iYear % 4 == 0 && iYear % 100 != 0)
	{
		return true;
	}
	else if(iYear % 400 == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}