/**
 * Created by Rainc on 2015/4/20.
 */
function lan_en(){
  return{
    userName: 'User Name',
    password: 'Password',
    changePwd: 'Change Password',
    'The two passwords differ!': "The two passwords differ!"
  }
}