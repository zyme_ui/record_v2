/**
 * Created by Rainc on 15-1-6.
 */

var lan = window.location.search.split('?')[1];

$(document).ready(function() {

  $("#userName").kendoMaskedTextBox({
  });

  $("#userPwd").kendoMaskedTextBox({
  });

  $("#login").kendoButton({
    click: onLoginClick
  });

  $("#language").kendoMenu({
    select: onLanguageSelect
  });
  document.onkeydown=function(event){
    if (event.keyCode == 13){
      onLoginClick();
    }
  }
});

function onLoginClick(e){
  var userName = $('#userName').val(),
    userPwd =  $.md5($('#userPwd').val());
  $.ajax({
    url:'/api?action=getPassword&userPwd=' + userPwd + '&userName=' + userName,
    type:'get',
    complete: function(XHR, TS){
      if(XHR.status == 200){
        var res_text = JSON.parse(XHR.responseText);
        if(res_text.success == true){
          $.cookie('userName',userName);
          $.cookie('userPwd', userPwd);
          location.href = "index.html?" + lan;
        }else{
          window.alert(res_text.code);
        }
      }else{
        window.alert('网络连接出错！');
      }
    }
  })
}

function onLanguageSelect(e){
//  console.log($(e.item).children(".k-link").text());
  if($(e.item).children(".k-link").text() == 'English' || $(e.item).children(".k-link").text() == '英文'){
    location.href = "loginEN.html?EN"
  }else if($(e.item).children(".k-link").text() == 'Chinese' || $(e.item).children(".k-link").text() == '中文'){
    location.href = "loginCN.html?CN"
  }
}