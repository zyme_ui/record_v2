/**
 * Created by Rainc on 15-1-10.
 */

var fs = require('fs');
var crypto = require('crypto');
var querystring = require('querystring');
var http = require('http')
var config = require('./etc/config.json');

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

exports.reSendTo = function(data,cb){
//  fs.open(status_path,'r',function(err){
//    if(err){
//      console.log(err);
//      cb(err);
//    }else{
//      fs.readFile(status_path, 'utf8', function (err, data) {
//        if (err) {
//          throw err;
//          cb(err);
//        }else if(data){
          var num = 0;
          for(var i in data){
            num++;
          }
  if(num != 0){
    try{
      //  send to remote
      var postData = {
        Action:'UpdateStatusInfo',
        Version: data.product_info.version,
        Timestamp: format(new Date().getTime(),'yyyyMMddHHmmss'),
        Expires: format(new Date().getTime() + 300000,'yyyyMMddHHmmss'),
        serverUuid: server_id,
        productId: product_id,
        Signature: '',
        extendInfo:JSON.stringify(data)
      };
      md5Str = 'Action='+ postData.Action + '&ServerUuid=' + postData.serverUuid + '&Version=' + postData.Version + '&Timestamp=' + postData.Timestamp +
        '&Expires=' + postData.Expires;
      postData.Signature = crypto.createHash('md5').update(md5Str).digest('hex');

      var postData = querystring.stringify(postData);
      var options = {
        hostname: config.cloudServerHost,
        port: config.cloudServerPort,
        path: config.cloudServerPath ,
        method: 'POST',
        headers: {
          "Content-Type": 'application/x-www-form-urlencoded',
          "Content-Length": postData.length
        }
      };
      var req = http.request(options, function(res) {
        if(res.statusCode  == 200){
          res.setEncoding('utf8');
          res.on('data', function (chunk) {
          });
        }else{
          res.send(500,"error")
        }
      });
      req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
        cb(e);
      });
      req.write(postData + "\n");
      req.end();
      cb(null);
    }catch (e){
      logger.error(e);
      cb(e)
    }
  }
//        }
//      });
//    }
//  })
}

var format = function(time,format){
  var a = new Date(time);
  a.setTime(a.getTime() - 28800000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) ;
        break;
      case 'MM':
        return tf(t.getMonth() + 1) ;
        break;
      case 'mm':
        return tf(t.getMinutes()) ;
        break;
      case 'dd':
        return tf(t.getDate()) ;
        break;
      case 'HH':
        return tf(t.getHours()) ;
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}