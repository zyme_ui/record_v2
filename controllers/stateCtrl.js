/**
 * Created by Rainc on 2015/4/17.
 */

var global = require('./global.js').global;
var mysql1 = require('./basedao.js');
var async = require('async');
var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

tape1_serial_num = 1;   //流水号
dev1_serial_num = 1;   //流水号

exports.insertIntoTable = function(){
  //初始化时，插入一笔数据
  setTimeout(function(){
    //服务器初始化时查询表中最大的流水号
    var sql = 'select max(serial_no) as max_serial_no from tbl_pmd_tape_1';
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(data.length != 0){
        if(data[0].max_serial_no == config.tape_count_limit){
          tape1_serial_num = 1;
        }else{
          tape1_serial_num = data[0].max_serial_no + 1;
        }
        insert_sys_1();
      }
    })
    sql = 'select max(serial_no) as max_serial_no from tbl_device';
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(data.length != 0){
        if(data[0].max_serial_no == config.tape_count_limit){
          dev1_serial_num = 1;
        }else{
          dev1_serial_num = data[0].max_serial_no + 1;
        }
        insert_dev_1();
      }
    })
    //同时检查是否超过频率，超过则进行告警
    alarm_check();
  },10000)

  //每隔1分钟，判断tbl_pmd_tape_15数据量是否大于设置值，大于或等于的话重头开始进行更新，否则插入一笔数据
  setInterval(function(){
    insert_sys_1();
    insert_dev_1();
    //同时检查是否超过频率，超过则进行告警
    alarm_check();
  },60 * 1000)
}

function insert_sys_1(){
  var data =  GLOBAL.g_app_status;
  data.runtime.memoryUtilization = 100 - parseInt((data.runtime.free_memory/data.runtime.total_memory)*100) ;    //内存占用率
  data.runtime.diskUtilization = parseInt(parseInt(data.runtime.disk_used)/(parseInt(data.runtime.disk_used) + parseInt(data.runtime.disk_available))*10000)/100; //录音磁盘占用率
  data.runtime.cpu_utilization = parseInt(data.runtime.cpu_utilization * 100) ;   //CPU使用率
  data.runtime.rx_rate =  parseInt(data.runtime.rx_rate * 100)/100 ;  //网络接收流量
  data.runtime.tx_rate =  parseInt(data.runtime.tx_rate * 100)/100 ;  //网络发送流量
  data.runtime.loadavg_1m = parseInt(data.runtime.loadavg_1m * 100)/100 ;   //CPU1分钟平均负载
  data.runtime.loadavg_5m = parseInt(data.runtime.loadavg_5m * 100)/100 ;   //CPU5分钟平均负载
  data.runtime.loadavg_15m = parseInt(data.runtime.loadavg_15m * 100)/100 ;   //CPU15分钟平均负载
  data.files_count = data.files_count || 0;
  //检测该服务器数据是否大于设置值
  var sql = "select count(*) as total from tbl_pmd_tape_1 where domain_uuid=" + server_domain_uuid;
  //console.log(sql);
  mysql1.find(sql,function(err,rows){
    if(err){
      logger.error("查询服务器tbl_pmd_tape_1数据数出错！");
      logger.error(err);
    }else{
      if(rows[0].total < parseInt(config.tape_count_limit)){
        //判断该流水号的数据是否存在，存在就更新，不存在就插入
        sql = 'select * from tbl_pmd_tape_1 where domain_uuid=' + server_domain_uuid + ' and serial_no=' + tape1_serial_num;
        mysql1.find(sql,function(err,row){
          if(err){
            logger.error("查询服务器tbl_pmd_tape_1数据数出错！");
            logger.error(err);
          }else if(row.length == 0){
            sql = "insert into tbl_pmd_tape_1 (domain_uuid,rec_status,sys_uuid,serial_no,generate_time,sessions_count,his_sessions_count," +
            "his_success_count,his_failed_count,files_count,recordings_size,loadavg_1m,loadavg_5m,loadavg_15m,cpuUtilization,memoryUtilization,diskUtilization,rx_rate,tx_rate) values(" +
            server_domain_uuid + "," + 0 + "," + data.product_info.server_id + "," + tape1_serial_num + ",'" + global.localStaticToUTC(new Date(),'yyyyMMddHHmmss') + "'," + data.app_status.sessions_count + ","
            + data.app_status.processed_total + ","  + data.app_status.processed_succeed + ","  + data.app_status.processed_failed + "," + data.files_count +
            "," + data.runtime.recordings_size + ",'" + data.runtime.loadavg_1m + "','" + data.runtime.loadavg_5m + "','" + data.runtime.loadavg_15m + "','" + data.runtime.cpu_utilization + "','" + data.runtime.memoryUtilization + "','" + data.runtime.diskUtilization + "','" + data.runtime.rx_rate +
            "','" + data.runtime.tx_rate + "')";
            //console.log(sql);
            mysql1.process(sql);
          }else{
            sql = "update tbl_pmd_tape_1 set generate_time='" + global.localStaticToUTC(new Date(),'yyyyMMddHHmmss') + "',sessions_count=" +  data.app_status.sessions_count
            + ",his_sessions_count=" + data.app_status.processed_total + ",his_success_count=" + data.app_status.processed_succeed + ",his_failed_count=" + data.app_status.processed_failed + ",files_count=" + data.files_count +
            ",recordings_size=" + data.runtime.recordings_size + ",loadavg_1m='" + data.runtime.loadavg_1m + "',loadavg_5m='" + data.runtime.loadavg_5m + "',loadavg_15m='" + data.runtime.loadavg_15m  +
            "',cpuUtilization='" + data.runtime.cpu_utilization + "',memoryUtilization='" + data.runtime.memoryUtilization +
            "',diskUtilization='" + data.runtime.diskUtilization + "',rx_rate='" + data.runtime.rx_rate + "',tx_rate='" + data.runtime.tx_rate + "' where domain_uuid=" + server_domain_uuid  +
            " and serial_no=" + tape1_serial_num;
            //console.log(sql);
            mysql1.process(sql);
          }
          tape1_serial_num ++;
        })
      }else{
        //判断若数据总数超过设置值，则清空数据并重始流水号并插入初始数据，否则循环更新数据
        if(rows[0].total > parseInt(config.tape_count_limit)){
          sql = "delete from tbl_pmd_tape_1 where domain_uuid=" + server_domain_uuid ;
          mysql1.process(sql);
          tape1_serial_num = 1;
        }else{
          if(tape1_serial_num - 1 == parseInt(config.tape_count_limit)){
            tape1_serial_num = 1;
          }

          sql = "update tbl_pmd_tape_1 set generate_time='" + global.localStaticToUTC(new Date(),'yyyyMMddHHmmss') + "',sessions_count=" +  data.app_status.sessions_count
          + ",his_sessions_count=" + data.app_status.processed_total + ",his_success_count=" + data.app_status.processed_succeed + ",his_failed_count=" + data.app_status.processed_failed + ",files_count=" + data.files_count +
          ",recordings_size=" + data.runtime.recordings_size + ",loadavg_1m='" + data.runtime.loadavg_1m + "',loadavg_5m='" + data.runtime.loadavg_5m + "',loadavg_15m='" + data.runtime.loadavg_15m  +
          "',cpuUtilization='" + data.runtime.cpu_utilization + "',memoryUtilization='" + data.runtime.memoryUtilization +
          "',diskUtilization='" + data.runtime.diskUtilization + "',rx_rate='" + data.runtime.rx_rate + "',tx_rate='" + data.runtime.tx_rate + "' where domain_uuid=" + server_domain_uuid  +
          " and serial_no=" + tape1_serial_num;
          //console.log(sql);
          mysql1.process(sql);
          tape1_serial_num ++;
        }
      }
    }
  })
}

function insert_dev_1(){
  //对g_ip解析
  var mtgs = [] ;
  for(var i in GLOBAL.g_app_status.app_status.g_ip){
    mtgs.push({name:i,value:GLOBAL.g_app_status.app_status.g_ip[i]})
  }
  async.eachSeries(mtgs,function(item,cb){
    //检测该设备数据是否大于设置值
    var sql = "select count(*) as total from tbl_device where domain_uuid=" + server_domain_uuid + " and dev_ip='" + item.name + "'";
    mysql1.find(sql,function(err,rows){
      if(err){
        logger.error("查询tbl_device出错！");
        logger.error(err);
      }else{
        if(rows[0].total < parseInt(config.tape_count_limit)){
          sql = "select * from tbl_device where domain_uuid=" + server_domain_uuid + " and dev_ip='" + item.name + "' and serial_no=" + dev1_serial_num;
          mysql1.find(sql,function(err,row){
            if(err){
              logger.error("查询tbl_device出错！");
              logger.error(err);
            }else if(row.length == 0){
              sql = "insert into tbl_device (domain_uuid,rec_status,dev_ip,serial_no,generate_time,his_sessions_count,his_success_count," +
              "his_failed_count) values(" + server_domain_uuid + "," +
              0 + ",'" + item.name + "'," + dev1_serial_num + ",'" + global.localStaticToUTC(new Date().getTime()) + "'," +
              parseInt(item.value['g_processed_total']) + "," + parseInt(item.value['g_processed_succeed']) + "," + parseInt(item.value['g_processed_failed'])  + ")";
              //console.log(sql);
              mysql1.process(sql);
            }else{
              sql = "update tbl_device set generate_time='" + global.localStaticToUTC(new Date().getTime()) + "',his_sessions_count=" +
              parseInt(item.value['g_processed_total']) + ",his_success_count=" + parseInt(item.value['g_processed_succeed']) + ",his_failed_count=" + parseInt(item.value['g_processed_failed'])
              + " where domain_uuid=" + server_domain_uuid + " and dev_ip='" + item.name  + "' and serial_no=" + dev1_serial_num;
              //console.log(sql);
              mysql1.process(sql);
            }
            dev1_serial_num ++;
          })
        }else{
          //判断若数据总数超过设置值，则清空数据并重始流水号并插入初始数据，否则循环更新数据
          if(rows[0].total > parseInt(config.tape_count_limit)){
            sql = "delete from tbl_device where domain_uuid=" + server_domain_uuid + " and dev_ip=" + item.name;
            mysql1.process(sql);
            dev1_serial_num = 1;
          }else{
            if(dev1_serial_num - 1 == parseInt(config.tape_count_limit)){
              dev1_serial_num = 1;
            }
            sql = "update tbl_device set generate_time='" + global.localStaticToUTC(new Date().getTime()) + "',his_sessions_count=" +
            parseInt(item.value['g_processed_total']) + ",his_success_count=" + parseInt(item.value['g_processed_succeed']) + ",his_failed_count=" + parseInt(item.value['g_processed_failed'])
            + " where domain_uuid=" + server_domain_uuid + " and dev_ip='" + item.name  + "' and serial_no=" + dev1_serial_num;
            //console.log(sql);
            mysql1.process(sql);
            dev1_serial_num ++;
          }
        }
      }
    })
  },function(err){});
}

function alarm_check(){
  if(config.cpuAlarmRate != 0 && GLOBAL.g_app_status.runtime.cpu_utilization > config.cpuAlarmRate){
    //判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=4 and confirmed=1 and create_time < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(data.length == 0){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,4,'CPU_ALARM','"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('CPU alarm error!' + err);
          }
        })
      }
    })
  }
  if(config.serverMemoryAlarmRate != 0 && GLOBAL.g_app_status.runtime.memoryUtilization > config.serverMemoryAlarmRate){
    //判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=5 and confirmed=1 and create_time < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(data.length == 0){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,5,'SERVER_MEMORY_ALARM','"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('server memory alarm error!' + err);
          }
        })
      }
    })
  }
  if(config.diskUsedAlarmRate != 0 && GLOBAL.g_app_status.runtime.diskUtilization > config.diskUsedAlarmRate){
    //判断在配置时间内无已告警未确认项，无则插入
    var sql = "select * from tbl_alarm where alarm_object=6 and confirmed=1 and create_time < DATE_SUB(UTC_TIMESTAMP(),INTERVAL 30 MINUTE)";
    mysql1.find(sql,function(err,data){
      if(err){
        logger.error(err);
      }else if(data.length == 0){
        sql = "insert into tbl_alarm (level,alarm_object,alarm_cause,create_time) values(4,6,'SERVER_DISK_ALARM','"
        + global.localStaticToUTC(new Date().getTime()) + "')";
        mysql1.Cb_process(sql,null,function(err){
          if(err){
            logger.error('server disk alarm error!' + err);
          }
        })
      }
    })
  }
}