/**
 * Created by Rainc on 15-3-3.
 */
var fs = require('fs');
var async = require('async');
var global = require('./global.js').global;
var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

var dateObj = {};            //用于缓存监听到的文件

//获取文件信息，存储在内存中
exports.saveToMemory = function(){
  fs.readdir(recording_path,function(err,files){
    if(err){
      logger.error(err);
    }else{
      var arr = [];
      var reg = new RegExp(/^\d{4}(\-|\/|\.)\d{1,2}(\-|\/|\.)\d{1,2}$/);
      for(var i in files){
        if(reg.test(files[i])){
          arr.push({fileName: files[i],filePath:''})
        }
      }
      GLOBAL.recordDirList = arr;  //目录信息
      GLOBAL.recordList = {};  //录音文件信息
      async.forEach(arr,function(items,cb){
        var dirPath = items.fileName + '/';
        //监听目录,用于监听启动时存在的录音文件目录
        fs.exists(recording_path + "/" + dirPath, function (exists) {
          if(exists){
            fs.watch(recording_path + "/" + dirPath,function(sec_event,sec_filename){
              //console.log('event is: ' + sec_event + ' filename:' + sec_filename);
              if(sec_event == 'change' || sec_event == 'rename'){
                if(sec_filename){
                  //读取该目录下的文件信息，更新缓存
                  if(dateObj[sec_filename] == undefined){
                    dateObj[sec_filename] = items.fileName;   //记录该文件所在目录
                    fs.stat(recording_path + "/" + dirPath + sec_filename,function(err,stats){
                      if(stats){
                        var name = sec_filename.split('_');
                        var timestamp = name[0];
                        var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                        var MTG_PORT = name[5];
                        var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                        var caller = name[8];
                        var called = name[9].split('.')[0];
                        //解析时间戳获取日期
                        var date = new Date(parseInt(timestamp)).format("yyyy-MM-dd hh:mm:ss").substring(0,10);
                        if(GLOBAL.recordList[date] == undefined){
                          GLOBAL.recordList[date] = [];
                        }
                        GLOBAL.recordList[date].push({fileName: sec_filename,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:dirPath,parentFileName:items.fileName,fileCtime:global.localToUTC(new Date().setTime(parseInt(timestamp))),fileSize:parseInt(stats.size/1024)})
                      }else{
                        for(var i=0;i<GLOBAL.recordList[items.fileName].length;i++){
                          if(GLOBAL.recordList[items.fileName][i].fileName == sec_filename){
                            GLOBAL.recordList[items.fileName].splice(i,1)
                          }
                        }
                      }
                    })
                  }
                }
              }
            })
          }
        });
        //读取文件信息
        addFileDetail( dirPath,items.fileName,function(){
          cb(null);
        });
      },function(err){
        if(err == null){
          console.log('文件信息缓存成功！');
        //console.log(GLOBAL.recordList,GLOBAL.recordDirList);
          //10秒后计算当前录音文件数
          setTimeout(function(){
            if(GLOBAL.g_app_status.runtime){
              GLOBAL.g_app_status.runtime.files_count = 0;
              for(var i in GLOBAL.recordList){
                GLOBAL.g_app_status.runtime.files_count += GLOBAL.recordList[i].length
              }
            }
          },10 * 1000)
        }
      })
    }
  })

  //启动定时器，定时将已成的录音文件缓存删除
  setInterval(function(){
    for(var i in dateObj){
      fs.stat(recording_path + "/" + dateObj[i] + '/' + i,function(err,stats){
        if(stats){
          var mtime = stats.mtime;
          var timestamp = new Date(mtime).getTime();
          //判断是否超过10分钟，是则删除
          if(new Date().getTime() - timestamp > 60 * 1000){
            delete dateObj[i]
          }
        }else{
          //.tmp文件生成为可播放文件后，删除.tmp缓存
          delete dateObj[i]
        }
      })
    }
  },60*1000)
}

//启动监听目录文件改动程序，用于生成，删除录音文件目录
exports.watchFile = function(){
  fs.watch(recording_path, function (event, filename) {
    //console.log('event is: ' + event + ' filename:' + filename);
    if(event == 'rename' || event == 'change'){
      //判断文件目录中是否已经存在这个目录同时符合日期格式
      var reg = new RegExp(/^\d{4}(\-|\/|\.)\d{1,2}(\-|\/|\.)\d{1,2}$/);
      //console.log(filename,reg.test(filename))
      if(reg.test(filename)){
        //清空文件目录缓存后，重新加载该目录缓存
        GLOBAL.recordDirList = [];
        fs.readdir(recording_path,function(err,file){
          if(!err){
            for(var key =0;key< file.length;key++){
              //判断文件目录是否存在，不存在则添加缓存
              var isExistDir = false;
              for(var i in GLOBAL.recordDirList){
                if(GLOBAL.recordDirList[i].fileName == file[key]){
                  isExistDir = true;
                  break;
                }
              }
              if(!isExistDir){
                GLOBAL.recordDirList.push({fileName: file[key],filePath:''});
              }
            }
          }
        })
        //判断是否存在这个目录，若不存在，则进行监听
        //var isExistDir = false;
        //for(var i in GLOBAL.recordDirList){
        //  if(GLOBAL.recordDirList[i].fileName == filename){
        //    isExistDir = true;
        //    break;
        //  }
        //}
        //if(!isExistDir){
          var dirPath = filename + '/';
          //判断路径是否存在
          fs.exists(recording_path + "/" + dirPath, function (exists) {
            if(exists){
              fs.watch(recording_path + "/" + dirPath,function(sec_event,sec_filename){
                //console.log('event is: ' + sec_event);
                if(sec_event == 'change' || sec_event == 'rename'){
                  if(sec_filename){
                    if(!dateObj[sec_filename]){
                      dateObj[sec_filename] = filename;   //记录该文件所在目录
                      fs.stat(recording_path + "/" + dirPath + sec_filename,function(err,stats){
                        if(stats){
                          var name = sec_filename.split('_');
                          var timestamp = name[0];
                          var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                          var MTG_PORT = name[5];
                          var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                          var caller = name[8];
                          var called = name[9].split('.')[0];
                          //解析时间戳获取日期
                          var date = new Date(parseInt(timestamp)).format("yyyy-MM-dd hh:mm:ss").substring(0,10);
                          if(GLOBAL.recordList[date] == undefined){
                            GLOBAL.recordList[date] = [];
                          }
                          GLOBAL.recordList[date].push({fileName: sec_filename,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:dirPath,parentFileName:filename,fileCtime:global.localToUTC(new Date().setTime(parseInt(timestamp))),fileSize:parseInt(stats.size/1024)})
                        }else{
                          for(var i=0;i<GLOBAL.recordList[filename].length;i++){
                            if(GLOBAL.recordList[filename][i].fileName == sec_filename){
                              GLOBAL.recordList[filename].splice(i,1)
                            }
                          }
                        }
                      })
                    }
                  }
                }
              })
            }
          });
        //}
      }
    }
  });
}

function addFileDetail(dirPath,fileName,callback){
  fs.readdir(recording_path + "/" + dirPath,function(err,file){
    if(err){
      logger.error(err);
    }else{
      var arr = [];
      var timestampObj = {};    //记录时间戳，用于双通道文件，避免添加多次
      async.forEach(file,function(item,cb){
        //过滤掉正在录的文件
        if(item.indexOf('.') == 0){
          cb(null);
        }else{
          fs.stat(recording_path + "/" + dirPath + item,function(err,stats){
            if(stats){
              var name = item.split('.')[0].split('_');
              if(!timestampObj[name[0]]){
                var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                var MTG_PORT = name[5];
                var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                var caller = name[9];
                var called = name[10];
                arr.push({fileName: item,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:dirPath,parentFileName:fileName,fileCtime:global.localStaticToUTC(new Date().setTime(stats.ctime)),fileSize:parseInt(stats.size/1024)})
                timestampObj[name[0]] = true;
              }
            }
            cb(null);
          })
        }
      },function(err){
        GLOBAL.recordList[fileName] = arr;
        //计算当前录音文件数
        if(GLOBAL.g_app_status.runtime){
          GLOBAL.g_app_status.runtime.files_count = 0;
          for(var i in GLOBAL.recordList){
            GLOBAL.g_app_status.runtime.files_count += GLOBAL.recordList[i].length
          }
        }
        callback(null);
      })
    }
  })
}