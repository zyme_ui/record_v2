/**
 * Created by Rainc on 15-1-6.
 */

var fs = require('fs');
//var loadCache = require('../appServer');
var sendTo= require('../sendToRemote');
var async = require('async');
var mysql1 = require('./basedao.js');
var lanCon = require('./lanCon.js');
var crypto = require('crypto');
var exec = require('child_process').exec;
var global = require('./global.js').global;
var db = require('./db_pool.js');
var file_manager = require('../file_manager.js');

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

exports.recordApi = function(req,res){
  var action = req.query.action || req.body.action;
  if(!action || action == ''){
    res.json({success:false,result:{},errMsg:'require param action!'})
  }else{
    var cache = [];
    var data = {};
    var num = 0;
    num++;
    for(var i in GLOBAL.g_app_status){
      num++;
      //创建新的数据结构
      if(typeof(GLOBAL.g_app_status[i]) == 'object'){
        if(!data[i]){
          data[i] = {};
        }
        for(var j in GLOBAL.g_app_status[i]){
          data[i][j] = GLOBAL.g_app_status[i][j]
        }
      }else{
        data[i] = GLOBAL.g_app_status[i];
      }
    }
    if(data && data.runtime){
      data.timestamp = global.localStaticToUTC(data.timestamp,'yyyyMMddHHmmss');
      data.runtime.system_uptime = global.localStaticToUTC(data.runtime.system_uptime,'yyyyMMddHHmmss');
      data.runtime.process_uptime = global.localStaticToUTC(data.runtime.process_uptime,'yyyyMMddHHmmss');
      data.runtime.process_up_days = parseInt((new Date().getTime() - new Date(data.runtime.process_uptime).getTime())/(24*60*60*1000));
      data.runtime.memoryUtilization = 100 - parseInt((data.runtime.free_memory/data.runtime.total_memory)*100) ;    //内存占用率
      data.runtime.total_memory = parseInt(data.runtime.total_memory) ;
      data.runtime.loadavg_1m = parseInt(data.runtime.loadavg_1m * 100)/100 ;   //CPU1分钟平均负载
      data.runtime.loadavg_5m = parseInt(data.runtime.loadavg_5m * 100)/100 ;   //CPU5分钟平均负载
      data.runtime.loadavg_15m = parseInt(data.runtime.loadavg_15m * 100)/100 ;   //CPU15分钟平均负载
      data.runtime.cpu_utilization = parseInt(data.runtime.cpu_utilization * 100) ;   //CPU占用率
      data.runtime.cpu_count = parseInt(data.runtime.cpu_count) ;   //CPU个数
      data.runtime.rx_rate =  parseInt(data.runtime.rx_rate * 100)/100 ;  //网络接收流量
      data.runtime.tx_rate =  parseInt(data.runtime.tx_rate * 100)/100 ;  //网络发送流量
      data.runtime.diskUtilization = parseInt(parseInt(data.runtime.disk_used)/(parseInt(data.runtime.disk_used) + parseInt(data.runtime.disk_available))*10000)/100; //录音磁盘占用率
    }
    if(num != 0){
      //判断是否登录，未登录直接跳转到登录界面
      if(!req.session.isVisit && action != 'loginAction' && action != 'getValidRecord'){
        return  res.json({success:false,result:{},errMsg:'Session timeout!Please relogin!'})
      }
      try{
        switch(action){
          case 'checkSeriousExist':
            //判断服务是否已经连接上数据库，若未连接上，直接回复数据库连接错误
            if(config.connected_to_database){
              var sql = "select * from tbl_alarm where level=5 and confirmed =1 and domain_uuid=" + server_domain_uuid;
              mysql1.find(sql,function(err,data){
                if(err){
                  logger.error(err);
                  res.json({success:false,result:{},errMsg:'Database error!'})
                }else if(data.length != 0){
                  res.json({success:true,result:{exist:true},errMsg:""})
                }else{
                  res.json({success:true,result:{exist:false},errMsg:""})
                }
              })
            }else{
              res.json({success:false,result:{},errMsg:{code:'ECONNREFUSED'}})
              //同时尝试重新连接
              db.handleDisconnect(function(err,con){});
            }
            break;
          case 'getValidRecord':
            //提供录音文件搜索功能，与话单服务器对接
            try{
              var caller = req.body.caller;
              var called = req.body.called;
              var recordId = req.body.recordId;
              var cmd = "find " + recording_path + " -name *_" + recordId + "_" + caller + "_" + called + ".wav";
              exec(cmd,function(error, stdout, stderr){
                if(error){
                  logger.error(error);
                  res.json({success:false,result:{},errMsg:"Exec error!"})
                }else{
                  var files = stdout.split('\n');
                  var list = [];
                  for(var i=0;i<files.length-1;i++){
                    var fileName = files[i].split('/')[4];
                    var fileDir = files[i].split('/')[3];
                    list.push({fileName:fileName,fileDir:fileDir});
                  }
                  res.json({success:true,result:{list:list},errMsg:""})
                }
              })
            }catch(e){
              logger.error(e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'getPerformanceStatistic':
            var dateRange = req.body.dateRange || '1 HOUR';
            var sql = "select loadavg_1m,loadavg_5m,loadavg_15m,cpuUtilization,memoryUtilization,diskUtilization," +
              "rx_rate,tx_rate,generate_time from tbl_pmd_tape_1 where domain_uuid=" +  server_domain_uuid
              + " and generate_time >= DATE_SUB(UTC_TIMESTAMP(),INTERVAL " + dateRange + ") order by generate_time asc";
            mysql1.find(sql,function(err,data){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:"Database error!"});
              }else{
                res.json({success:true,result:data,errMsg:""});
              }
            })
            break;
          case 'getPerformanceCurrentStatistic':
            try{
              var values = [{
                memoryUtilization: data.runtime.memoryUtilization,
                cpuUtilization: data.runtime.cpu_utilization,
                diskUtilization: data.runtime.diskUtilization,
                rx_rate: data.runtime.rx_rate,
                tx_rate: data.runtime.tx_rate,
                loadavg_1m: data.runtime.loadavg_1m,
                loadavg_5m: data.runtime.loadavg_5m,
                loadavg_15m: data.runtime.loadavg_15m,
                generate_time: new Date(global.localStaticToUTC(new Date().getTime()))
              }]
              res.json({success:true,result:values,errMsg:""});
            }catch(e){
              logger.error(e);
              res.json({success:false,result:{},errMsg:"Server operation error!"});
            }
            break;
          case 'getServerConfiguration':
            var values ={
              server_ip:config.server_ip,
              server_port:config.server_port,
              web_port:config.web_port,
              db_host:config.db_host,
              db_user:config.db_user,
              db_passwd:global.base64_encode(config.db_passwd),
              db_database:config.db_database,
              max_sessions:config.max_sessions,
              cpuAlarmRate:config.cpuAlarmRate,
              serverMemoryAlarmRate:config.serverMemoryAlarmRate,
              diskUsedAlarmRate:config.diskUsedAlarmRate,
              connect_to_cloud:config.connect_to_cloud,
              cloudServerHost:config.cloudServerHost,
              cloudServerPort:config.cloudServerPort,
              cloudServerPath:config.cloudServerPath,
              server_id:config.server_id
            }
            res.json({success:true,result:values,errMsg:""})
            break;
          case 'saveServerConfiguration':
            try{
              var obj ={};
              obj.server_ip = req.body.server_ip;
              obj.server_port = req.body.server_port;
                obj.web_port = req.body.web_port;
                obj.db_host = req.body.db_host;
                obj.db_user = req.body.db_user;
                obj.db_passwd = global.base64_decode(req.body.db_passwd);
                obj.db_database = req.body.db_database;
                obj.max_sessions = req.body.max_sessions;
                obj.cpuAlarmRate = req.body.cpuAlarmRate;
                obj.serverMemoryAlarmRate = req.body.serverMemoryAlarmRate;
                obj.diskUsedAlarmRate = req.body.diskUsedAlarmRate;
                obj.connect_to_cloud = req.body.connect_to_cloud;
                obj.cloudServerHost = req.body.cloudServerHost;
                obj.cloudServerPort = req.body.cloudServerPort;
                obj.cloudServerPath = req.body.cloudServerPath;
                obj.server_id = req.body.server_id;
              //循环修改config设置
              for(var i in obj){
                //更新config值
                config[i] = obj[i];
                //特殊处理路径
                if(i == 'cloudServerPath'){
                  obj[i] = obj[i].replace(/\//g,'\\\\/')
                }
                var cmd = 'sed -i "s/.*\\"' + i +  '\\":.*/  \\"' + i + '\\": \\"' + obj[i] + '\\",/" ' + config.serverPath + '/etc/config.json';
                exec(cmd,function(error, stdout, stderr){
                  if(error){
                    logger.error('exec error: ' + error);
                  }
                });
              }
              res.json({success:true,result:{},errMsg:""})
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'restartNow':
            try{
              var cmd = 'pm2 restart ' + config.serverPath + '/app.js';
              exec(cmd,function(error, stdout, stderr){
                if(error){
                  logger.error('exec error: ' + error);
                }
              });
              res.json({success:true,result:{},errMsg:''})
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          //case 'getFileUrl':
          //  var src = req.query.src;
          //  res.json({success:true,code: recording_path + '/' + src})
          //  break;
          //case 'sendToCloud':
          //  sendTo.reSendTo(data,function(err){
          //    //处理完成后修改服务器状态
          //    var sql = "update tbl_sys set dm_run_status="
          //    if(err){
          //      res.json({success:false,code:err})
          //      sql += "6 where uuid=" + data.product_info.server_id;
          //    }else{
          //      res.json({success:true,code:0})
          //      sql += "3 where uuid=" + data.product_info.server_id;
          //    }
          //    mysql1.Cb_process(sql,null,function(err){
          //      if(err){
          //        logger.error('更新服务器状态失败！');
          //        logger.error(err);
          //      }else{
          //        logger.info('更新服务器状态成功！');
          //      }
          //    })
          //  })
          //  break;
          case 'getRecordList':
            var dir = req.query.dir || req.body.dir;
            if(dir){
              if(GLOBAL.recordList[dir]){
                res.json(GLOBAL.recordList[dir])
              }else{
                res.json([])
              }
            }else{
              res.json(GLOBAL.recordDirList);
            }
            break;
          case 'deleteRecordDir':
            //删除录音文件目录及其下所有文件
            try{
              var items = req.body.items;
              var cmd = 'rm -rf ';
              if(typeof(items) == 'object'){
                for(var i=0;i<items.length;i++){
                  cmd += recording_path + '/' + items[i] + ' '
                  delete GLOBAL.recordList[items[i]]
                }
              }else{
                cmd += recording_path + '/' + items
                delete GLOBAL.recordList[items]
              }
              exec(cmd,function(error, stdout, stderr){
                if(error){
                  logger.error('exec error: ' + error);
                }
              });
              res.json({success:true,result:{},errMsg:''})
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'emptyRecordDir':
            //删除所有目录和文件
            try{
              var cmd = 'rm -rf ' + recording_path + '/*';
              GLOBAL.recordList = {};
              GLOBAL.recordDirList = [];
              exec(cmd,function(error, stdout, stderr){
                if(error){
                  logger.error('exec error: ' + error);
                }
              });
              res.json({success:true,result:{},errMsg:''})
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'playRecordItem':
            var fileName = req.query.fileName;
            var parentFileName = req.query.parentFileName;
            var out_format = req.query.out_format || 'wav';
            var chan_filenames = [fileName+'_0.'+out_format,fileName+'_1.'+out_format];
            file_manager.get_playable_file(chan_filenames,recording_path+'/'+parentFileName+'/',out_format,{cache_dir:config.cache_path},function(err,data){
              if(err){
                logger.error("get_playable_file failed:"+err.stack)
              }else{
                res.send(data);
                res.end();
              }
            })
            //file_manager.get_playable_file(chan_filenames,recording_path+'/'+parentFileName+'/',out_format,{cache_dir:config.cache_path}).then(function(data) {
            //  res.send(data);
            //}).catch(function(err) {
            //  logger.error("get_playable_file failed:"+err.stack)
            //});
            break;
          case 'download':
            var filePath = req.query.filePath || req.body.filePath;
            var fileName = req.query.fileName || req.body.fileName;
            var out_format = req.query.out_format || 'wav';
            var chan_filenames = [fileName+'_0.'+out_format,fileName+'_1.'+out_format];
            file_manager.get_playable_file(chan_filenames,recording_path + "/" + filePath,out_format,{cache_dir:config.cache_path},function(err,data){
              if(err){
                logger.error("get_playable_file failed:"+err.stack)
              }else{
                res.download(config.cache_path + '/' + fileName + '.' + out_format);
              }
            })
            break;
          case 'deleteRecordItems':
            //删除录音文件
            try{
              var items = req.body.items;
              var cmd = 'rm -rf ';
              if(typeof(items) == 'object'){
                for(var i=0;i<items.length;i++){
                  cmd += recording_path + '/' + items[i] + ' '
                }
              }else{
                cmd += recording_path + '/' + items
              }
              exec(cmd,function(error, stdout, stderr){
                if(error){
                  logger.error('exec error: ' + error);
                }
              });
              res.json({success:true,result:{},errMsg:''})
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'emptyRecordItems':
            //删除所有目录和文件
            try{
              var dirName = req.body.dirName;
              var cmd = 'rm -rf ' + recording_path + '/' + dirName + '/*';
              exec(cmd,function(error, stdout, stderr){
                if(error){
                  logger.error('exec error: ' + error);
                }
              });
              res.json({success:true,result:{},errMsg:''})
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'searchRecord':
            //目前仅在record_path目录下搜索
            try{
              var labelValue = req.body.labelValue;
              var selectValue = req.body.selectValue;
              var textValue = req.body.textValue;
              var dirName = req.body.dirName;
              var ip,ip_1,ip_2,ip_3,ip_4,cmd,port;
              var filePath = recording_path + '/' + dirName;
              if(labelValue && selectValue && textValue){
                if(labelValue == 1){
                  //搜索mtg_address
                  if(selectValue == 1){
                    if(textValue.indexOf(':') != -1){
                      ip = textValue.split(':')[0];
                      ip_1 = ip.split('.')[0];
                      ip_2 = ip.split('.')[1];
                      ip_3 = ip.split('.')[2];
                      ip_4 = ip.split('.')[3];
                      port = textValue.split(':')[1];
                      if(ip_1 && ip_2 && ip_3 && ip_4){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_" + ip_2 + "_"
                        + ip_3 + "_" + ip_4  + "_" + port + "_*";
                      }else if(!ip_2){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_*_*_*_" + port + "_*";
                      }else if(!ip_3){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_" + ip_2 + "_*_*_" + port + "_*";
                      }else if(!ip_4){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_" + ip_2 + "_"
                        + ip_3 + "_*_" + port + "_*";
                      }
                    }else{
                      textValue = textValue.replace(/\./g,'_');
                      cmd = "find " + filePath + " -name *_" + textValue + "_*";
                    }
                  }else if(selectValue == 2){
                    //包含
                    if(textValue.indexOf(':') != -1){
                      ip = textValue.split(':')[0];
                      ip_1 = ip.split('.')[0];
                      ip_2 = ip.split('.')[1];
                      ip_3 = ip.split('.')[2];
                      ip_4 = ip.split('.')[3];
                      port = textValue.split(':')[1];
                      if(ip_1 && ip_2 && ip_3 && ip_4){
                        cmd = "find " + filePath  + " -name *_" + ip_1 + "_" + ip_2 + "_"
                        + ip_3 + "_" + ip_4  + "_" + port + "_*";
                      }else if(!ip_2){
                        cmd = "find " + filePath  + " -name *_" + ip_1 + "_*_*_*_" + port + "_*";
                      }else if(!ip_3){
                        cmd = "find " + filePath  + " -name *_" + ip_1 + "_" + ip_2 + "_*_*_" + port + "_*";
                      }else if(!ip_4){
                        cmd = "find " + filePath  + " -name *_" + ip_1 + "_" + ip_2 + "_"
                        + ip_3 + "_*_" + port + "_*";
                      }
                    }else if(textValue.indexOf('.') != -1){
                      ip_1 = textValue.split('.')[0];
                      ip_2 = textValue.split('.')[1];
                      ip_3 = textValue.split('.')[2];
                      ip_4 = textValue.split('.')[3];
                      if(ip_1 && ip_2 && ip_3 && ip_4){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_" + ip_2 + "_"
                        + ip_3 + "_" + ip_4  + "_*_*";
                      }else if(!ip_2){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_*_*_*_*_*";
                      }else if(!ip_3){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_" + ip_2 + "_*_*_*_*";
                      }else if(!ip_4){
                        cmd = "find " + filePath + " -name *_" + ip_1 + "_" + ip_2 + "_"
                        + ip_3 + "_*_*_*";
                      }
                    }else{
                      textValue = textValue.replace(/\./g,'_');
                      cmd = "find " + filePath + " -name *_" + textValue + "_*";
                    }
                  }
                }else if(labelValue == 2){
                  if(selectValue == 1){
                    cmd = "find " + filePath + " -name *_*_*_*_*_*_*_*_" + textValue + "_*.*";
                  }else{
                    cmd = "find " + filePath + " -name *_*_*_*_*_*_*_*_*" + textValue + "*_*.*";
                  }
                }else if(labelValue == 3){
                  if(selectValue == 1){
                    cmd = "find " + filePath + " -name *_*_*_*_*_*_*_*_*_" + textValue + ".*";
                  }else{
                    cmd = "find " + filePath + " -name *_*_*_*_*_*_*_*_*_*" + textValue + "*.*";
                  }
                }
                //console.log(cmd)
                exec(cmd,{maxBuffer:2*1024*1024},function(error, stdout, stderr){
                  if(error){
                    logger.error(error);
                    res.json({success:false,result:{},errMsg:"Exec error!"})
                  }else{
                    var files = stdout.split('\n');
                    files.pop();   //删除数组末尾空项
                    var list = [];
                    async.forEach(files,function(item,cb){
                      var fileName = item.split('/')[4];
                      var fileDir = item.split('/')[3];
                      fs.stat(item,function(err,stats){
                        if(stats){
                          var name = fileName.split('_');
                          var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                          var MTG_PORT = name[5];
                          var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                          var caller = name[8];
                          var called = name[9].split('.')[0];
                          list.push({fileName: fileName,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:fileDir + '/',parentFileName:fileDir,fileCtime:global.localStaticToUTC(new Date().setTime(stats.ctime),'yyyyMMddHHmmss'),fileSize:parseInt(stats.size/1024)})
                        }
                        cb(null);
                      })
                    },function(err){
                      if(!err){
                        res.json({success:true,result:{list:list},errMsg:""})
                      }
                    })
                  }
                })
              }else{
                //当没有参数时，查询这个目录下的所有录音文件
                res.json({success:true,result:{list:GLOBAL.recordList[dirName]},errMsg:""})
              }
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:"Exec error!"})
            }
            break;
          case 'getAlarmLog':
            var search = req.body.search;
            var limit = req.body.limit;
            var order = req.body.order;
            var offset = req.body.offset;
            var sort = req.body.sort || 'create_time';
            var sql = 'select count(*) from tbl_alarm where domain_uuid=' + server_domain_uuid;
            if(search){
              if(search == lanCon.getLanValue('debug')){
                sql += ' and level=1'
              }else if(search == lanCon.getLanValue('notice')){
                sql += ' and level=2'
              }else if(search == lanCon.getLanValue('warning')){
                sql += ' and level=3'
              }else if(search == lanCon.getLanValue('error')){
                sql += ' and level=4'
              }else if(search == lanCon.getLanValue('serious')){
                sql += ' and level=5'
              }else if(search == lanCon.getLanValue('unknown')){
                sql += ' and alarm_object=0'
              }else if(search == lanCon.getLanValue('database')){
                sql += ' and alarm_object=1'
              }else if(search == lanCon.getLanValue('cloud')){
                sql += ' and alarm_object=2'
              }else if(search == lanCon.getLanValue('system service')){
                sql += ' and alarm_object=3'
              }else if(search == lanCon.getLanValue('cpu userate')){
                sql += ' and alarm_object=4'
              }else if(search == lanCon.getLanValue('server memory userate')){
                sql += ' and alarm_object=5'
              }else if(search == lanCon.getLanValue('disk userate')){
                sql += ' and alarm_object=6'
              }else if(search == lanCon.getLanValue('confirmed')){
                sql += ' and confirmed=2'
              }else if(search == lanCon.getLanValue('unconfirmed')){
                sql += ' and confirmed=1'
              }else{
                //用于界面显示查询结果为空
                sql += ' and level=999'
              }
            }
            mysql1.find(sql,function(err,data){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:"Database error!"})
              }else{
                var total  = data[0]['count(*)'];
                var sql = 'select * from tbl_alarm where domain_uuid=' +  server_domain_uuid;
                if(search){
                  if(search == lanCon.getLanValue('debug')){
                    sql += ' and level=1'
                  }else if(search == lanCon.getLanValue('notice')){
                    sql += ' and level=2'
                  }else if(search == lanCon.getLanValue('warning')){
                    sql += ' and level=3'
                  }else if(search == lanCon.getLanValue('error')){
                    sql += ' and level=4'
                  }else if(search == lanCon.getLanValue('serious')){
                    sql += ' and level=5'
                  }else if(search == lanCon.getLanValue('unknown')){
                    sql += ' and alarm_object=0'
                  }else if(search == lanCon.getLanValue('database')){
                    sql += ' and alarm_object=1'
                  }else if(search == lanCon.getLanValue('cloud')){
                    sql += ' and alarm_object=2'
                  }else if(search == lanCon.getLanValue('system service')){
                    sql += ' and alarm_object=3'
                  }else if(search == lanCon.getLanValue('cpu userate')){
                    sql += ' and alarm_object=4'
                  }else if(search == lanCon.getLanValue('server memory userate')){
                    sql += ' and alarm_object=5'
                  }else if(search == lanCon.getLanValue('disk userate')){
                    sql += ' and alarm_object=6'
                  }else if(search == lanCon.getLanValue('confirmed')){
                    sql += ' and confirmed=2'
                  }else if(search == lanCon.getLanValue('unconfirmed')){
                    sql += ' and confirmed=1'
                  }else{
                    sql += ' and level=999'
                  }
                }
                sql += ' order by ' + sort + ' ' + order + ' limit ' + limit + ' offset '+ offset;
                mysql1.find(sql,function(err,data1){
                  if(err){
                    logger.error(err);
                    res.json({success:false,result:{},errMsg:err});
                  }else{
                    res.json({success:true,result:{list:data1,total:total},errMsg:""});
                  }
                });
              }
            })
            break;
          case 'confirmAlarms':
            var alarms = req.body.alarms;
            var sql =  'update tbl_alarm set confirmed=2,confirm_time="' + global.localStaticToUTC(new Date().getTime()) +  '" where uuid in (';
            if(typeof(alarms) == 'string'){
              sql += alarms + ')';
            }else{
              for(var i =0;i< alarms.length -1;i++){
                sql += alarms[i] + ','
              }
              sql += alarms[alarms.length -1] + ")";
            }
            mysql1.Cb_process(sql,null,function(err){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:"Database error!"})
              }else{
                res.json({success:true,result:{},errMsg:""})
              }
            });
            break;
          case 'deleteAlarms':
            var alarms = req.body.alarms;
            var sql =  'delete from tbl_alarm where uuid in (';
            if(typeof(alarms) == 'string'){
              sql += alarms + ')';
            }else{
              for(var i =0;i< alarms.length -1;i++){
                sql += alarms[i] + ','
              }
              sql += alarms[alarms.length -1] + ")";
            }
            mysql1.Cb_process(sql,null,function(err){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:'Database error!'})
              }else{
                res.json({success:true,result:{},errMsg:""})
              }
            });
            break;
          case 'emptyAlarms':
            var sql =  'delete from tbl_alarm where domain_uuid=' + server_domain_uuid;
            mysql1.Cb_process(sql,null,function(err){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:'Database error!'})
              }else{
                res.json({success:true,result:{},errMsg:""})
              }
            });
            break;
          case 'getServiceStatistic':
            var dateRange = req.body.dateRange || '1 HOUR';
            var sql = "select * from tbl_pmd_tape_1 where domain_uuid=" + server_domain_uuid
              + " and generate_time >= DATE_SUB(UTC_TIMESTAMP(),INTERVAL " + dateRange + ") order by generate_time asc";
            mysql1.find(sql,function(err,data){
              if(err){
                logger.error(err)
                res.json({success:false,result:{},errMsg:'Database error!'});
              }else{
                res.json({success:true,result:data,errMsg:""});
              }
            });
            break;
          case 'getServiceCurrentStatistic':
            var data = GLOBAL.g_app_status;
            var values = [{
              sessions_count:data.app_status.sessions_count,
              his_sessions_count:data.app_status.processed_total,
              his_success_count:data.app_status.processed_succeed,
              his_failed_count:data.app_status.processed_failed,
              generate_time: new Date(global.localStaticToUTC(new Date().getTime()))
            }];
            res.json({success:true,result:values,errMsg:""});
            break;
          case 'getDeviceList':
            var search = req.body.search;
            var limit = req.body.limit;
            var order = req.body.order;
            var offset = req.body.offset;
            var sort = req.body.sort || 'generate_time';
            var sql = 'select dev_ip from tbl_device where domain_uuid=' + server_domain_uuid ;
            if(search){
              sql += ' and generate_time like "%' + search + '%" or dev_ip like "%' + search + '%"';
            }
            sql +=  ' group by dev_ip';
            mysql1.find(sql,function(err,data){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:'Database error!'})
              }else{
                var total  = data.length;
                var sql = 'select * from tbl_device where domain_uuid=' + server_domain_uuid;
                if(search){
                  sql += ' and generate_time like "%' + search + '%" or dev_ip like "%' + search + '%"';
                }
                sql += ' group by dev_ip' + ' order by ' + sort + ' ' + order + ' limit ' + limit + ' offset '+ offset  ;
                mysql1.find(sql,function(err,data1){
                  if(err){
                    logger.error(err);
                    res.json({success:false,result:{},errMsg:err});
                  }else{
                    res.json({success:true,result:{list:data1,total:total},errMsg:""});
                  }
                });
              }
            })
            break;
          case 'getDeviceStatistic':
            var deviceIP = req.body.deviceIP;
            var sql = "select * from tbl_device where domain_uuid=" + server_domain_uuid + " and dev_ip='" + deviceIP + "'";
            mysql1.find(sql,function(err,data){
              if(err){
                logger.error(err);
                res.json({success:false,result:{},errMsg:'Database error!'});
              }else{
                res.json({success:true,result:data,errMsg:""});
              }
            })
            break;
          case 'getRecordPack':
            //备份文件目录
            dirPath = backup_path;
            fs.readdir(dirPath,function(err,files){
              if(err){
                logger.error(err);
              }else{
                var arr = [];
                async.forEachSeries(files,function(item,cb){
                  fs.stat(dirPath + item,function(err,stats){
                    if(stats){
                      var name = item.split('_');
                      var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                      var MTG_PORT = name[5];
                      var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                      var caller = name[8];
                      var called = name[9].split('.')[0];
//                        item = name[0] + '.wav'
                      arr.push({fileName: item,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:'',fileCtime:global.localStaticToUTC(new Date().setTime(stats.ctime),'yyyyMMddHHmmss'),fileSize:parseInt(stats.size/1024)})
                    }
                    cb(null)
                  })
                },function(err){
                  if(!err){
                    res.json(arr);
                  }
                })
              }
            })
            break;
          //case 'deleteFile':
          //  var fileName = req.query.fileName;
          //  var filePath = req.query.filePath;
          //  if(!fileName){
          //    res.json({success:false,code:'Lack Data(fileName)'})
          //  }else{
          //    if(filePath != ''){
          //      fs.unlink(recording_path + '/' + filePath + '/' + fileName,function(err){
          //        if(err){
          //          logger.error(err);
          //          res.json({success:false,code:err})
          //        }else{
          //          res.json({success:true,code:0})
          //          //同时删除缓存
          //          //for(var i in GLOBAL.recordList){
          //          //  if((i + '/') == filePath){
          //          //    for(var j in GLOBAL.recordList[i]){
          //          //      if(GLOBAL.recordList[i][j].fileName == fileName){
          //          //        GLOBAL.recordList[i].remove(GLOBAL.recordList[i][j])
          //          //      }
          //          //    }
          //          //  }
          //          //}
          //        }
          //      })
          //    }else{
          //      fs.unlink(backup_path + '/' + fileName,function(err){
          //        if(err){
          //          logger.error(err);
          //          res.json({success:false,code:err})
          //        }else{
          //          res.json({success:true,code:0})
          //        }
          //      })
          //    }
          //  }
          //  break;
//          case 'saveSetting':
////            var listen_ip = req.body.listen_ip;
////            var listen_port = req.body.listen_port;
//            var server_version = req.body.server_version;
//            var server_id = req.body.server_id;
//            var product_name = req.body.product_name;
//            var product_id = req.body.product_id;
//            if(!server_version ||!server_id ||!product_name || !product_id){
//              res.json({success:false,code:'Lack of incoming parameters'})
//            }else{
////              GLOBAL.g_app_status.listen_ip = listen_ip;
////              GLOBAL.g_app_status.listen_port = listen_port;
//              GLOBAL.g_app_status.server_version = server_version;
//              GLOBAL.g_app_status.server_id = server_id;
//              GLOBAL.g_app_status.product_name = product_name;
//              GLOBAL.g_app_status.product_id = product_id;
//              res.json({success:true,code:0})
//            }
//            break;
          case 'loginAction':
            var rcUserName = req.body.rcUserName || req.query.rcUserName;
            var rcPassword = req.body.rcPassword || req.query.rcPassword;
            localLan = req.body.localLan || req.query.localLan;        //设置全局语言
            if(rcUserName != config.account ){
              res.json({success:false,result:{},errMsg:lanCon.getLanValue('login_failed_userName')})
            }else if(crypto.createHash('md5').update(rcPassword).digest('hex').toUpperCase() != config.password){
              res.json({success:false,result:{},errMsg:lanCon.getLanValue('login_failed_password')})
            }else{
              req.session.isVisit = 1;
              res.json({success:true,result:{},errMsg:""});
            }
            break;
          case 'logoutAction':
            req.session.isVisit = null;
            res.json({success:true,result:{},errMsg:""});
            break;
          case 'changePwd':
            try{
              var password = config.password;
              var account = config.account;
              var oldPwd = req.query.oldPwd || req.body.oldPwd;
              var newPwd = req.query.newPwd || req.body.newPwd;
              if(!oldPwd || !newPwd ){
                res.json({success:false,result:{},errMsg:lanCon.getLanValue('Missing Parameters!')})
              }else if(crypto.createHash('md5').update(oldPwd).digest('hex').toUpperCase() != password){
                res.json({success:false,result:{},errMsg:lanCon.getLanValue('Old password incorrect!')})
              }else{
                config.password = crypto.createHash('md5').update(newPwd).digest('hex').toUpperCase();
                //同时修改配置文件
                var cmd = 'sed -i "s/.*\\"password\\":.*/  \\"password\\": \\"' + config.password + '\\",/" ' + config.serverPath + '/etc/config.json';
                exec(cmd,function(error, stdout, stderr){
                  if(error){
                    logger.error('exec error: ' + error);
                  }
                });
                res.json({success:true,result:{},errMsg:''});
              }
            }catch(e){
              logger.error('exec error: ' + e);
              res.json({success:false,result:{},errMsg:'Exec error!'})
            }
            break;
          default:
            res.json({success:false,result:{},errMsg:'not valid action'})
        }
      }catch(err){
        logger.error(err);
        res.json({success:false,result:{},errMsg:'Unkown error!'})
      }
    }
  }
}

Array.prototype.remove = function(s) {
  for (var i = 0; i < this.length; i++) {
    if (typeof(s) == 'object'){
      if (s == this[i]){
        this.splice(i, 1);
        break;
      }
    }else if(typeof(s) == 'number'){
      if (s == i){
        this.splice(i, 1);
        break;
      }
    }
  }
}

Date.prototype.format = function(format){
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(), //day
    "h+" : this.getHours(), //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3), //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  }

  for(var k in o) {
    if(new RegExp("("+ k +")").test(format)) {
      format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    }
  }
  return format;
};
